# Quiz Master

## Stack

* React + Redux
* Webpack 2 w/ HMR
* Sass
* Eslint + Stylelint
* Jest 
* Node.js Development APIs

## Requirements

* Node.js 6.9+
* npm 3+ or yarn

## Installation and Usage

```
git clone https://github.com/dwightjack/quiz-master.git
cd quiz-master
yarn install #or npm install
```

### Run in development mode

```
yarn start #or npm start
```

### Run in production mode

```
yarn run build:production #or npm run build:production
yarn run server #or npm run server
```

### URLs

The Quiz user interface is available at http://localhost:8000

The admin are is available at http://localhost:8000/admin

### Run tests

```
yarn test #or npm test
```

