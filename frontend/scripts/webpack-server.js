const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const address = require('ip').address();

const config = require('../config/webpack.dev');
const paths = require('../config/paths');
const setupAPI = require('./api.server');

const publicPath = 'http://' + address + ':8000' + paths.publicPath;

config.output.publicPath = publicPath;

config.entry.app.unshift(
    'eventsource-polyfill', // Necessary for hot reloading with IE
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://' + address + ':8000',
    'webpack/hot/only-dev-server'
);

config.plugins.unshift(
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin()
);

const server = new WebpackDevServer(webpack(config), {
    contentBase: paths.toAbsPath('dist.root'),
    compress: false,
    hot: true,
    historyApiFallback: true,
    //TODO: temporary fix for https://github.com/mxstbr/react-boilerplate/issues/370 and https://github.com/webpack/style-loader/pull/96
    publicPath,
    stats: 'normal',
    setup: setupAPI
});

server.listen(8000, (err) => {
    if (err) {
        console.log(err); //eslint-disable-line no-console
    }
    console.log('\nStarted a server at http://%s:8000\n', address); //eslint-disable-line no-console
});