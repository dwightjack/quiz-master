/**
 * Node-Sass Functions
 */
const times = require('lodash/times');

let types;

try {
    types = require('node-sass').types;  //eslint-disable-line global-require
} catch (e) {
    types = require('gulp-sass/node_modules/node-sass').types; //eslint-disable-line
}

module.exports = {

    'build-env()': () => new types.String(process.env.NODE_ENV),
    'map-to-JSON($map)': (map) => {
        const obj = {};
        times(map.getLength(), (i) => {
            const key = map.getKey(i).getValue().toString();
            obj[key] = map.getValue(i).getValue();
        });
        return new types.String(JSON.stringify(obj));
    }
};

