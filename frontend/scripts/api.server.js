const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const shortid = require('shortid');
const low = require('lowdb');
const bodyParser = require('body-parser');

const setupAPI = (app) => {
    const dbFilePath = path.join(__dirname, '../db/db.json');
    if (fs.existsSync(dbFilePath) === false) {
        fs.writeFileSync(dbFilePath, '');
    }
    const db = low(dbFilePath);
    const wordsToNumber = require('./utils').wordsToNumber; //eslint-disable-line global-require

    db.defaults({ questions: [], answers: [] }).value();

    let maxId = db.get('questions').map('id').max().value() || 1;

    app.use(bodyParser.json());

    app.get('/api/v1/questions', (req, res) => {
        const questions = db.get('questions').value();

        if (req.query.admin !== '1') {
            //filter out answers when not in admin mode
            res.json({
                data: questions.map((q) => _.omit(q, 'answer'))
            });
            return;
        }

        res.json({data: questions});
    });

    app.post('/api/v1/questions', (req, res) => {

        const questions = db.get('questions');

        const question = _.omit(req.body, '_uid');
        question.id = (++maxId); //eslint-disable-line no-plusplus
        const inserted = questions.push(question).last().value();

        res.json({ data: inserted });
    });

    app.put('/api/v1/questions/:id', (req, res) => {

        const id = parseInt(req.params.id, 10);

        const question = db.get('questions').find({ id }).value();
        const questionIdx = db.get('questions').findIndex({ id });
        const newQuestion = Object.assign(question, _.omit(req.body, '_uid'));

        db.get('questions').splice(questionIdx, 1, newQuestion);

        res.json({ data: newQuestion });
    });

    app.delete('/api/v1/questions/:id', (req, res) => {

        const question = db.get('questions').remove(({ id }) => id === parseInt(req.params.id, 10)).value();

        res.json({ data: question[0] });
    });



    app.post('/api/v1/answers', (req, res) => {

        if (_.isEmpty(req.body) || !Array.isArray(req.body)) {
            res.status(500).json({ data: 'You should provide some answers to store' });
            return;
        }

        const id = shortid.generate();
        const answers = req.body;

        //save them...
        db.get('answers').push({ id, answers }).value();

        //an easy access map
        const answersMap = answers.reduce((ret, { qid, answer }) => (
            Object.assign(ret, { [qid]: answer })
        ), {});

        const results = db.get('questions').map(({ id, body, answer }) => { //eslint-disable-line no-shadow

            const userAnswer = answersMap[id];
            var correct = false; //eslint-disable-line no-var

            if (/^[0-9]+$/.test(answer)) {
                correct = userAnswer === answer || wordsToNumber(userAnswer) === parseInt(answer, 10);
            } else {
                correct = userAnswer === answer;
            }


            return {
                qid: id,
                question: body,
                answer,
                userAnswer,
                correct
            };

        }).value();

        res.json({data: results});


    });
};

module.exports = setupAPI;