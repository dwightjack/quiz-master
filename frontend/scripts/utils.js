const glob = require('glob');
const path = require('path');
const paths = require('./../config/paths');

module.exports.getAssetPath = (searchpath) => {
    const filepath = glob.sync(paths.toAbsPath('dist.assets/' + searchpath)).pop();
    const folder = path.dirname(searchpath);
    return filepath ? (paths.toPath(folder) + '/' + path.basename(filepath)) : null;
};


const baseNumbers = {
    zero: 0,
    one: 1,
    two: 2,
    three: 3,
    four: 4,
    five: 5,
    six: 6,
    seven: 7,
    eight: 8,
    nine: 9,
    ten: 10,
    eleven: 11,
    twelve: 12,
    thirteen: 13,
    fourteen: 14,
    fifteen: 15,
    sixteen: 16,
    seventeen: 17,
    eighteen: 18,
    nineteen: 19,
    twenty: 20,
    thirty: 30,
    forty: 40,
    fifty: 50,
    sixty: 60,
    seventy: 70,
    eighty: 80,
    ninety: 90
};

const multipliers = {
    hundred: 100,
    thousand: 1000,
    million: 1000000,
    billion: 1000000000
};

module.exports.wordsToNumber = (str) => {

    const words = str.trim().toLowerCase().replace(/[^a-z-]+/, ' ').split(/[\s-]+/);

    return words.reduce(([small, big], word) => {
        if (baseNumbers[word] !== undefined) {
            small += baseNumbers[word]; //eslint-disable-line no-param-reassign
        } else if (multipliers[word] !== undefined) {
            big += small * multipliers[word]; //eslint-disable-line no-param-reassign
            small = 0; //eslint-disable-line no-param-reassign
        }
        return [small, big];
    }, [0, 0]).reduce((a, b) => a + b, 0);

};