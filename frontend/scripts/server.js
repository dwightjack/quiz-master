const path = require('path');
const express = require('express');
const history = require('connect-history-api-fallback');
const address = require('ip').address();
const setupAPI = require('./api.server');

const app = express();


setupAPI(app);
app.use(history());
app.use(express.static(path.join(process.cwd(), 'public')));

app.listen(8000, () => {
    console.log('Listening on http://' + address + ':8000');
});