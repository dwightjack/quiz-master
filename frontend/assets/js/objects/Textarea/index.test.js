import React from 'react';
import { shallow } from 'enzyme';
import Textarea from './index';

/* eslint-env jest */
describe('<Textarea />', () => {

    it('should render a <div> root tag', () => {

        const textarea = shallow(<Textarea />);

        expect(textarea.is('div')).toBe(true);

    });



    it('should render an <textarea> with a class `o-textarea__field` and a sibling <span>', () => {

        const textarea = shallow(<Textarea />);

        expect(textarea.find('textarea.o-textarea__field').length).toBe(1);
        expect(textarea.find('span').length).toBe(1);

    });



    it('should have a default class `o-textarea` on its root element', () => {

        const textarea = shallow(<Textarea />);
        expect(textarea.prop('className')).toBe('o-textarea');

    });



    it('should add `className` prop to the root element', () => {

        const textarea = shallow(<Textarea className="test-class" />);

        expect(textarea.prop('className')).toBe('o-textarea test-class');

    });



    it('should pass-through props to the input element except `className`', () => {

        const textarea = shallow(<Textarea className="test-class" id="test-id" disabled value="my-value" />);

        const textareaProps = textarea.find('textarea').props();

        expect(textareaProps.className).not.toMatch('test-class');
        expect(textareaProps.id).toBe('test-id');
        expect(textareaProps.value).toBe('my-value');
        expect(textareaProps.disabled).toBeDefined();

    });


});