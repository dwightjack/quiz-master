import React, { PropTypes } from 'react';
import omit from 'lodash/omit';
import classnames from 'classnames';

import './_textarea.scss';

const Textarea = (props) => {

    const { className } = props;
    const inputProps = omit(props, ['className']);

    return (
        <div className={classnames('o-textarea', className)}>
            <textarea className="o-textarea__field" {...inputProps} />
            <span />
        </div>
    );
};

Textarea.propTypes = {
    className: PropTypes.string
};
Textarea.defaultProps = {
    className: ''
};

export default Textarea;