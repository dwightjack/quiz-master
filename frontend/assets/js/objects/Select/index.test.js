import React from 'react';
import { shallow } from 'enzyme';
import FaChevronDown from 'react-icons/lib/fa/chevron-down';
import Select from './index';


/* eslint-env jest */
describe('<Select />', () => {



    it('should render a <div> root tag', () => {

        const select = shallow(<Select />);

        expect(select.is('div')).toBe(true);

    });


    it('should render an <select> with a class `o-select__field` and a sibling <FaChevronDown> component', () => {

        const select = shallow(<Select />);

        expect(select.find('select.o-select__field').length).toBe(1);
        expect(select.find(FaChevronDown).length).toBe(1);
        expect(select.find(FaChevronDown).prop('className')).toBe('o-select__ico');

    });


    it('should have a default class `o-select` on its root element', () => {

        const select = shallow(<Select />);
        expect(select.prop('className')).toBe('o-select');

    });


    it('should add `className` prop to the root element', () => {

        const select = shallow(<Select className="test-class" />);

        expect(select.prop('className')).toBe('o-select test-class');

    });


    it('should render a prop `options` as select <options>', () => {

        const options = [
            { label: 'label1', value: '1' },
            { label: 'label2', value: '2' }
        ];

        const select = shallow(<Select options={options} />);

        const optionsEls = select.find('option');

        expect(optionsEls.length).toBe(options.length);

        optionsEls.forEach((option, i) => {
            expect(option.prop('value')).toBe(options[i].value);
            expect(option.text()).toBe(options[i].label);
        });

    });


    it('should break when `options` are not and and array of objects', () => {

        expect(() => {
            shallow(<Select options={{ label: 'label1', value: '1' }} />);
        }).toThrow();

        expect(() => {
            shallow(<Select options={['array', 'of', 'strings']} />);
        }).toThrow();

    });



    it('renders an empty <option> with `emptyOption` prop', () => {

        const options = [
            { label: 'label1', value: '1' },
            { label: 'label2', value: '2' }
        ];

        const select = shallow(<Select options={options} emptyOption />);
        const optionsEls = select.find('option');

        expect(optionsEls.length).toBe(options.length + 1);
        expect(optionsEls.get(0).props.value).toBe('');
        expect(optionsEls.at(0).text()).toBe('');
    });


    it('should pass-through props to the input element except `className`, `options` and `emptyOption`', () => {

        const options = [];
        const select = shallow(<Select className="test-class" id="test-id" value="my-value" options={options} emptyOption />);

        const selectProps = select.find('select').props();

        expect(selectProps.className).not.toMatch('test-class');
        expect(selectProps.id).toBe('test-id');
        expect(selectProps.value).toBe('my-value');
        expect(selectProps.options).toBeUndefined();
        expect(selectProps.emptyOption).toBeUndefined();
    });


});