import React, { PropTypes } from 'react';
import omit from 'lodash/omit';
import classnames from 'classnames';
import FaChevronDown from 'react-icons/lib/fa/chevron-down';

import './_select.scss';

const Select = (props) => {

    const { className, options, emptyOption } = props;
    const selectProps = omit(props, ['className', 'options', 'emptyOption']);

    const optionsEls = options.map(({ label, value }) => {
        if (!label || !value) {
            throw new TypeError('You must provide both a `label` and a `value` for each option');
        }
        return (
            <option value={value} key={value}>
                {label}
            </option>
        );
    });

    return (
        <div className={classnames('o-select', className)}>
            <select className="o-select__field" {...selectProps}>
                {emptyOption && <option value="" />}
                {optionsEls}
            </select>
            <FaChevronDown className="o-select__ico" />
        </div>
    );
};

Select.propTypes = {
    emptyOption: PropTypes.bool,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string,
            label: PropTypes.string
        })
    ),
    className: PropTypes.string
};
Select.defaultProps = {
    options: [],
    emptyOption: false,
    className: ''
};

export default Select;