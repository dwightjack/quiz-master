import React, { PropTypes } from 'react';
import omit from 'lodash/omit';
import classnames from 'classnames';

import './_textinput.scss';

const TextInput = (props) => {

    const { className } = props;
    const inputProps = omit(props, ['className']);

    return (
        <div className={classnames('o-textinput', className)}>
            <input className="o-textinput__field" {...inputProps} />
            <span />
        </div>
    );
};

TextInput.propTypes = {
    className: PropTypes.string
};
TextInput.defaultProps = {
    type: 'text',
    className: ''
};

export default TextInput;