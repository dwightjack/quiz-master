import React from 'react';
import { shallow } from 'enzyme';
import TextInput from './index';

/* eslint-env jest */
describe('<TextInput />', () => {



    it('should render a <div> root tag', () => {

        const input = shallow(<TextInput />);

        expect(input.is('div')).toBe(true);

    });



    it('should render an <input> with a class `o-textinput__field` and a sibling <span>', () => {

        const input = shallow(<TextInput />);

        expect(input.find('input[type].o-textinput__field').length).toBe(1);
        expect(input.find('span').length).toBe(1);

    });



    it('should default to an input of type `text`', () => {

        const input = shallow(<TextInput />);
        expect(input.find('input').prop('type')).toBe('text');

    });



    it('should have a default class `o-textinput` on its root element', () => {

        const input = shallow(<TextInput />);
        expect(input.prop('className')).toBe('o-textinput');

    });



    it('should add `className` prop to the root element', () => {

        const input = shallow(<TextInput className="test-class" />);

        expect(input.prop('className')).toBe('o-textinput test-class');

    });



    it('should pass-through props to the input element except `className`', () => {

        const input = shallow(<TextInput className="test-class" id="test-id" disabled value="my-value" />);

        const inputProps = input.find('input').props();

        expect(inputProps.className).not.toMatch('test-class');
        expect(inputProps.id).toBe('test-id');
        expect(inputProps.value).toBe('my-value');
        expect(inputProps.disabled).toBeDefined();

    });


});