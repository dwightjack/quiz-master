import React, { PropTypes } from 'react';
import classnames from 'classnames';
import noop from 'lodash/noop';

import './_button.scss';

const Button = ({ type, className, children, disabled, onClick }) => (
    <button
        type={type}
        className={classnames('o-button', className)}
        disabled={disabled}
        onClick={onClick}
    >
        {children}
    </button>
);

Button.propTypes = {
    type: PropTypes.string,
    className: PropTypes.string,
    children: PropTypes.node,
    disabled: PropTypes.bool,
    onClick: PropTypes.func

};
Button.defaultProps = {
    type: 'button',
    onClick: noop,
    className: 'o-button--primary'
};

export default Button;