import React from 'react';
import { shallow } from 'enzyme';
import Button from './index';

/* eslint-env jest */
describe('<Button>', () => {

    it('should render a <button> tag with type button', () => {

        const button = shallow(<Button />);

        expect(button.is('button[type="button"]')).toBe(true);
    });



    it('should add a `.o-button.o-button--primary` className by default', () => {

        const button = shallow(<Button />);

        expect(button.is('.o-button.o-button--primary')).toBe(true);
    });



    it('should accept a `className` prop on the root element', () => {

        const testClass = 'custom-class';
        const button = shallow(<Button className={testClass} />);

        expect(button.is(`.${testClass}`)).toBe(true);
    });

    it('should preserve the base className `o-button` when a custom `className` prop is passed-in', () => {

        const button = shallow(<Button className="customClass" />);

        expect(button.is('.o-button')).toBe(true);

    });



    it('should accept a `type` prop on the root element', () => {

        const button = shallow(<Button type="submit" />);

        expect(button.is('button[type="submit"]')).toBe(true);
    });



    it('should accept a `disabled` boolean prop on the root element', () => {

        const button = shallow(<Button disabled />);

        expect(button.is('button[disabled]')).toBe(true);

    });



    it('should accept a `onClick` event handler', () => {

        const spy = jest.fn();
        const button = shallow(<Button onClick={spy} />);

        button.simulate('click');

        expect(spy).toHaveBeenCalled();
    });



    it('should render child elements', () => {

        const button = shallow(<Button><p>Child</p></Button>);

        expect(button.find('p').length).toBe(1);
    });

});