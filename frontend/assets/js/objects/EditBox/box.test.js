import React from 'react';
import { shallow } from 'enzyme';

import EditBox from './box';

/* eslint-env jest */
describe('<EditBox />', () => {

    let editBox;

    beforeEach(() => {
        editBox = shallow(<EditBox />);
    });

    it('should render a <form> tag', () => {
        expect(editBox.is('form')).toBe(true);
    });


    it('should have a default class `.o-edit-box` on root element', () => {
        expect(editBox.prop('className')).toBe('o-edit-box');
    });


    it('should add `className` to the root element', () => {

        const className = 'my-class';
        const expected = `o-edit-box ${className}`;
        const customEditBox = shallow(<EditBox className={className} />);

        expect(customEditBox.prop('className')).toBe(expected);

    });


    it('should disable client-side validation', () => {
        expect(editBox.prop('noValidate')).toBeDefined();
    });


    it('should render and `id` prop on the root element', () => {

        const id = 'my-edit-box';
        const customEditBox = shallow(<EditBox id={id} />);

        expect(customEditBox.prop('id')).toBe(id);

    });


    it('should render `label` prop inside a legend field', () => {

        const label = 'This is a label';
        const customEditBox = shallow(<EditBox label={label} />);

        expect(customEditBox.find('legend').text()).toBe(label);

    });


    it('should accept children and place them inside a <fieldset>', () => {

        const customEditBox = shallow(<EditBox><p>A Child</p></EditBox>);
        const fieldset = customEditBox.find('fieldset');

        expect(fieldset.find('p').matchesElement(<p>A Child</p>)).toBe(true);

    });


    it('should accept an `onSave` handler on form root element', () => {

        const spy = jest.fn();
        const customEditBox = shallow(<EditBox onSubmit={spy} />);

        customEditBox.simulate('submit');

        expect(spy).toHaveBeenCalled();

    });


});