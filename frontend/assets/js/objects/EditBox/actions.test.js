import React from 'react';
import { shallow } from 'enzyme';

import Actions from './actions';

/* eslint-env jest */
describe('<Actions />', () => {


    it('should render a <footer> with a class `o-edit-box__actions`', () => {

        const actions = shallow(<Actions />);

        expect(actions.is('footer.o-edit-box__actions')).toBe(true);

    });


    it('should add className to the root element', () => {

        const className = 'my-classname';
        const expected = `o-edit-box__actions ${className}`;
        const actions = shallow(<Actions className={className} />);

        expect(actions.prop('className')).toBe(expected);

    });

    it('should accept children elements', () => {

        const actions = shallow(<Actions><p>A child</p></Actions>);

        expect(actions.find('p').length).toBe(1);

    });


});