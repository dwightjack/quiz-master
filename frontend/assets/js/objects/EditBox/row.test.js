import React from 'react';
import { shallow } from 'enzyme';

import Row from './row';

/* eslint-env jest */
describe('<Row />', () => {

    it('should render a <div> with a class `o-edit-box__row`', () => {

        const row = shallow(<Row />);

        expect(row.is('div.o-edit-box__row')).toBe(true);

    });


    it('should add className to the root element', () => {

        const className = 'my-classname';
        const expected = `o-edit-box__row ${className}`;
        const row = shallow(<Row className={className} />);

        expect(row.prop('className')).toBe(expected);

    });


    it('should accept children elements', () => {

        const row = shallow(<Row><p>A child</p></Row>);

        expect(row.find('p').length).toBe(1);

    });

});