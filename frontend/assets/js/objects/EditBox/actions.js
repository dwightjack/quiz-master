import React, { PropTypes } from 'react';
import classnames from 'classnames';

const Actions = ({ className, children }) => (
    <footer className={classnames('o-edit-box__actions', className)}>
        {children}
    </footer>
);
Actions.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node
};
Actions.defaultProps = {};

export default Actions;