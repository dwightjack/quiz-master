import React, { PropTypes } from 'react';
import classnames from 'classnames';

const Label = ({ className, children, id }) => (
    <label htmlFor={id} className={classnames('o-edit-box__label', className)}>
        {children}
    </label>
);

Label.propTypes = {
    className: PropTypes.string,
    id: PropTypes.string,
    children: PropTypes.node
};
Label.defaultProps = {};

export default Label;