import React from 'react';
import { shallow } from 'enzyme';

import Field from './field';

/* eslint-env jest */
describe('<Field />', () => {


    it('should render a <div> with a class `o-edit-box__field`', () => {

        const field = shallow(<Field />);

        expect(field.is('div.o-edit-box__field')).toBe(true);

    });


    it('should add className to the root element', () => {

        const className = 'my-classname';
        const expected = `o-edit-box__field ${className}`;
        const field = shallow(<Field className={className} />);

        expect(field.prop('className')).toBe(expected);

    });


    it('should accept children elements', () => {

        const field = shallow(<Field><p>A child</p></Field>);

        expect(field.find('p').length).toBe(1);

    });


});