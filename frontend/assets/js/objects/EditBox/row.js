import React, { PropTypes } from 'react';
import classnames from 'classnames';

const Row = ({ className, children }) => (
    <div className={classnames('o-edit-box__row', className)}>
        {children}
    </div>
);

Row.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node
};
Row.defaultProps = {};

export default Row;