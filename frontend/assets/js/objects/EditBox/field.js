import React, { PropTypes } from 'react';
import classnames from 'classnames';

const Field = ({ className, children }) => (
    <div className={classnames('o-edit-box__field', className)}>
        {children}
    </div>
);

Field.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node
};
Field.defaultProps = {};

export default Field;