import React, { PropTypes } from 'react';
import classnames from 'classnames';

const Box = ({ className, children, id, label, onSubmit }) => (
    <form id={id} className={classnames('o-edit-box', className)} method="post" noValidate onSubmit={onSubmit}>
        <fieldset className="o-edit-box__fieldset">
            <legend className="o-edit-box__legend">{label}</legend>
            {children}
        </fieldset>
    </form>
);

Box.propTypes = {
    className: PropTypes.string,
    id: PropTypes.string,
    onSubmit: PropTypes.func,
    label: PropTypes.string,
    children: PropTypes.node
};
Box.defaultProps = {};

export default Box;