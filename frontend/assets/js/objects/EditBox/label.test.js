import React from 'react';
import { shallow } from 'enzyme';

import Label from './label';

/* eslint-env jest */
describe('<Label />', () => {

    it('should render a <label> with a class `o-edit-box__label`', () => {

        const label = shallow(<Label />);

        expect(label.is('label.o-edit-box__label')).toBe(true);

    });


    it('should add className to the root element', () => {

        const className = 'my-classname';
        const expected = `o-edit-box__label ${className}`;
        const label = shallow(<Label className={className} />);

        expect(label.prop('className')).toBe(expected);

    });


    it('should render `id` prop as the `for` attribute instead of its the `id`', () => {

        const expected = 'my-id';
        const label = shallow(<Label id={expected} />);

        expect(label.prop('htmlFor')).toBe(expected);
        expect(label.prop('id')).toBeUndefined();

    });


    it('should accept children elements', () => {

        const label = shallow(<Label><p>A child</p></Label>);

        expect(label.find('p').length).toBe(1);

    });

});