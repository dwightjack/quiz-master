import React, { PropTypes } from 'react';
import classnames from 'classnames';
import RichTextEditor from 'react-rte';

import './_text-editor.scss';

export const DEFAULT_TOOLBAR_CONFIG = {
    display: ['INLINE_STYLE_BUTTONS', 'BLOCK_TYPE_BUTTONS', 'LINK_BUTTONS', 'HISTORY_BUTTONS'],
    INLINE_STYLE_BUTTONS: [
        { label: 'Bold', style: 'BOLD', className: 'custom-css-class' },
        { label: 'Italic', style: 'ITALIC' },
        { label: 'Underline', style: 'UNDERLINE' }
    ],

    BLOCK_TYPE_BUTTONS: [
        { label: 'UL', style: 'unordered-list-item' },
        { label: 'OL', style: 'ordered-list-item' }
    ]
};

const TextEditor = (props) => {

    const rteProps = Object.assign({}, props, {
        className: classnames('o-rte', props.className),
        toolbarClassName: 'o-rte__toolbar'
    });

    return (
        <RichTextEditor {...rteProps} />
    );
};

TextEditor.propTypes = {
    className: PropTypes.string,
    toolbarConfig: PropTypes.object //eslint-disable-line
};

TextEditor.defaultProps = {
    toolbarConfig: DEFAULT_TOOLBAR_CONFIG
};

export default TextEditor;