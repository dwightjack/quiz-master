import React from 'react';
import { shallow } from 'enzyme';
import RichTextEditor from 'react-rte';
import TextEditor, { DEFAULT_TOOLBAR_CONFIG } from './index';

/* eslint-env jest */
describe('<TextEditor>', () => {

    let textEditor;


    beforeEach(() => {
        textEditor = shallow(<TextEditor />);
    });


    it('should render an instance of react-rte', () => {
        expect(textEditor.is(RichTextEditor)).toBe(true);
    });


    it('should use a custom toolbar config by default', () => {
        expect(textEditor.prop('toolbarConfig')).toEqual(DEFAULT_TOOLBAR_CONFIG);
    });


    it('should accept a custom `toolbarConfig` prop', () => {
        const expected = { display: ['INLINE_STYLE_BUTTONS'] };
        const editor = shallow(<TextEditor toolbarConfig={expected} />);

        expect(editor.prop('toolbarConfig')).toEqual(expected);

    });


    it('should add a custom className to the root element', () => {

        expect(textEditor.prop('className')).toBe('o-rte');

        textEditor.setProps({ className: 'my-custom-classname' });
        expect(textEditor.prop('className')).toBe('o-rte my-custom-classname');

    });


    it('should pass-through other react-rte props', () => {

        const editor = shallow(<TextEditor autoFocus />);
        expect(editor.props()).toMatchObject({ autoFocus: true });

    });


});