import React from 'react';
import { shallow } from 'enzyme';
import Progress from './index';

/* eslint-env jest */
describe('<Progress>', () => {

    let progress;
    const current = 50;
    const total = 100;


    beforeEach(() => {
        progress = shallow(<Progress current={current} total={total} />);
    });


    it('should render a <div> tag', () => {
        expect(progress.is('div')).toBe(true);
    });


    it('should add `className` to the root <div> tag', () => {

        const className = 'my-class';
        progress.setProps({ className });
        expect(progress.prop('className')).toBe(`o-progress ${className}`);

    });


    it('should have `role="progressbar"`', () => {
        expect(progress.prop('role')).toBe('progressbar');
    });


    it('should have a numeric `valuemin`, ARIA attrs set to 0', () => {
        expect(progress.prop('aria-valuemin')).toBe(0);
    });


    it('should have a numeric `valuenow`, ARIA attrs set to `current` prop', () => {
        expect(progress.prop('aria-valuenow')).toBe(current);
        expect(progress.prop('aria-valuenow')).toEqual(expect.any(Number));
    });


    it('should have a numeric `valuemax`, ARIA attrs set to `total` prop', () => {
        expect(progress.prop('aria-valuemax')).toBe(total);
        expect(progress.prop('aria-valuemax')).toEqual(expect.any(Number));
    });


    it('should set its width to the completion percentage', () => {

        const expected = `${(current * 100) / total}%`;
        expect(progress.prop('style').width).toBe(expected);

        //update width
        progress.setProps({ current: total });
        expect(progress.prop('style').width).toBe('100%');

    });


});