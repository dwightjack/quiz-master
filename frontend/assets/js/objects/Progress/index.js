import React, { PropTypes } from 'react';
import classnames from 'classnames';

import './_progress.scss';

const Progress = ({ current, total, className }) => (
    <div
        className={classnames('o-progress', className)}
        role="progressbar"
        aria-valuenow={current}
        aria-valuemin={0}
        aria-valuetext={`Question ${current} of ${total}`}
        aria-valuemax={total}
        style={{ width: `${(current * 100) / total}%` }}
    />
);

Progress.propTypes = {
    className: PropTypes.string,
    current: PropTypes.number,
    total: PropTypes.number

};
Progress.defaultProps = {
    current: 0,
    total: 0
};

export default Progress;