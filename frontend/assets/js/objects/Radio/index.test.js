import React from 'react';
import { shallow } from 'enzyme';
import Radio from './index';

/* eslint-env jest */
describe('<Radio />', () => {

    it('should render a <div> root tag', () => {

        const radio = shallow(<Radio />);

        expect(radio.is('div')).toBe(true);

    });


    it('should render an <input> with a class `o-radio__field`', () => {

        const radio = shallow(<Radio />);

        expect(radio.find('input[type="radio"]').length).toBe(1);
        expect(radio.find('input[type="radio"]').prop('className')).toBe('o-radio__field');

    });


    it('should have a default class `o-radio` on its root element', () => {

        const radio = shallow(<Radio />);
        expect(radio.prop('className')).toBe('o-radio');

    });


    it('should add `className` prop to the root element', () => {

        const radio = shallow(<Radio className="test-class" />);

        expect(radio.prop('className')).toBe('o-radio test-class');

    });


    it('should render a <label class="o-radio__label"> when `label` prop has been supplied', () => {

        const radio = shallow(<Radio label="text" />);

        //nothing rendered
        expect(shallow(<Radio />).find('label').length).toBe(0);

        expect(radio.find('label').length).toBe(1);
        expect(radio.find('label').prop('className')).toBe('o-radio__label');

    });


    it('should render `label` as the label text', () => {

        const expectedText = 'text';
        const radio = shallow(<Radio label={expectedText} />);

        expect(radio.find('label').text()).toBe(expectedText);

    });


    it('should set a generated <input id> `id` and <label for>', () => {

        const expectedText = 'text';
        const radio = shallow(<Radio label={expectedText} />);

        const id = radio.find('input[type="radio"]').prop('id');

        expect(id).toBeDefined();
        expect(radio.find('label').prop('htmlFor')).toBe(id);

    });


    it('should set <input id> `id` and <label for> when `id` prop is provided', () => {

        const id = 'my-radio';
        const expectedText = 'text';
        const radio = shallow(<Radio id={id} label={expectedText} />);

        expect(radio.find('input[type="radio"]').prop('id')).toBe(id);
        expect(radio.find('label').prop('htmlFor')).toBe(id);

    });


    it('should pass-through props to the input element except `className` and `label`', () => {

        const radio = shallow(<Radio className="test-class" label="a label" value="my-value" />);

        const radioProps = radio.find('input').props();

        expect(radioProps.className).not.toMatch('test-class');
        expect(radioProps.value).toBe('my-value');
        expect(radioProps.label).toBeUndefined();

    });


});