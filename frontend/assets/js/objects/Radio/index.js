import React, { PropTypes } from 'react';
import omit from 'lodash/omit';
import uniqueId from 'lodash/uniqueId';
import classnames from 'classnames';

import './_radio.scss';

const Radio = (props) => {

    const { className, label, id = uniqueId('radio-') } = props;
    const inputProps = omit(props, ['className', 'label', 'id']);

    return (
        <div className={classnames('o-radio', className)}>
            <input id={id} className="o-radio__field" type="radio" {...inputProps} />
            {label && <label className="o-radio__label" htmlFor={id}>{label}</label>}
        </div>
    );
};

Radio.propTypes = {
    className: PropTypes.string,
    label: PropTypes.string,
    id: PropTypes.string
};
Radio.defaultProps = {
    className: ''
};

export default Radio;