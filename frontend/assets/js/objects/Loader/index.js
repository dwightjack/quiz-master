import React, { PropTypes } from 'react';
import classnames from 'classnames';

import './_loader.scss';

const Loader = ({ visible }) => (
    <div className={classnames('o-loader', { 'is-visible': visible })} aria-hidden={visible}>
        <svg className="o-loader__svg" width="80" height="80" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
            <circle cx="50" cy="50" r="40" stroke="rgba(190,56,18,0.13)" fill="none" strokeWidth="10" strokeLinecap="round" />
            <circle className="o-loader__spinner" cx="50" cy="50" r="40" stroke="#d16f18" fill="none" strokeWidth="6" strokeLinecap="round">
                <animate attributeName="stroke-dashoffset" dur="2s" repeatCount="indefinite" from="0" to="502" />
                <animate attributeName="stroke-dasharray" dur="2s" repeatCount="indefinite" values="150.6 100.4;1 250;150.6 100.4" />
            </circle>
        </svg>
    </div>
);
Loader.propTypes = {
    visible: PropTypes.bool
};
Loader.defaultProps = {
    visible: false
};

export default Loader;