import React from 'react';
import { shallow } from 'enzyme';
import Loader from './index';

/* eslint-env jest */
describe('<Loader>', () => {

    let loader;


    beforeEach(() => {
        loader = shallow(<Loader />);
    });


    it('should render a <div> tag with `o-loader` class by default', () => {
        expect(loader.is('div')).toBe(true);
        expect(loader.prop('className')).toBe('o-loader');
    });


    it('should toggle `is-visible` class accordingly to `visible` prop', () => {

        const visibleClass = 'is-visible';

        expect(loader.prop('className')).not.toContain(visibleClass);

        loader.setProps({ visible: true });
        expect(loader.prop('className')).toContain(visibleClass);

    });

    it('should toggle `aria-hidden` attr accordingly to `visible` prop', () => {

        expect(loader.prop('aria-hidden')).toBe(false);

        loader.setProps({ visible: true });
        expect(loader.prop('aria-hidden')).toBe(true);

    });


});