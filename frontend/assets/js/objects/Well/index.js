import React, { PropTypes } from 'react';
import classnames from 'classnames';

import './_well.scss';

const Well = ({ className, children }) => (
    <div className={classnames('o-well', className)}>
        {children}
    </div>
);

Well.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node
};

Well.defaultProps = {};

export default Well;