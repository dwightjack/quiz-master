import React from 'react';
import { shallow } from 'enzyme';
import Well from './index';

/* eslint-env jest */
describe('<Well>', () => {

    let well;


    beforeEach(() => {
        well = shallow(<Well />);
    });


    it('should render a <div> tag with `o-well` class by default', () => {
        expect(well.is('div')).toBe(true);
        expect(well.prop('className')).toBe('o-well');
    });


    it('should add `className` to the className of the root element', () => {

        const customClassName = 'my-class';
        const myWell = shallow(<Well className={customClassName} />);

        expect(myWell.prop('className')).toBe(`o-well ${customClassName}`);

    });


    it('should allow childrens', () => {

        const myWell = shallow(<Well><p>My child</p></Well>);
        expect(myWell.find('p').length).toBe(1);

    });

});