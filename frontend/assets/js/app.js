/**
 * Main Application File
 *
 * Use for bootstrapping large application
 * or just fill it with JS on small projects
 *
 */

import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';

import configureStore from './store';

import Root from './containers/Root';
import { loadingAction } from './shared/ui';
import api from './shared/api';



const store = configureStore();

api.connect(store, loadingAction);

// @see https://github.com/gaearon/react-hot-boilerplate/pull/61#issuecomment-254467020

ReactDOM.render(
    <AppContainer>
        <Root store={store} />
    </AppContainer>,
    document.getElementById('app-root')
);

if (__PRODUCTION__ === false) {

    window.store = store;

    if (module.hot) {

        module.hot.accept('./containers/Root', () => {

            const RootContainer = require('./containers/Root').default; //eslint-disable-line global-require

            ReactDOM.render(
                <AppContainer>
                    <RootContainer store={store} />
                </AppContainer>,
                document.getElementById('app-root')
            );
        });
    }
}