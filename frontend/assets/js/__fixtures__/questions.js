const questions = [
    {
        type: 'text',
        body: '<p>How many fingers has one hand?</p>',
        answer: '5',
        options: [],
        id: 1,
        _uid: 'string-uid'
    },
    {
        type: 'pick',
        body: '<p>When did the &nbsp;<em><strong>Battle of Sekigahara</strong></em> happen?</p>',
        answer: 'October 21, 1600',
        options: [
            'October 21, 1602',
            'May 10, 1604',
            'October 21, 1600',
            'October 20, 1600'
        ],
        id: 2,
        _uid: 'string-uid2'
    }
];

const emptyQuestion = {
    type: 'text',
    body: '',
    answer: '',
    options: []
};

export {
    questions,
    emptyQuestion
};