import React, { PureComponent, PropTypes } from 'react';
import FaFloppyO from 'react-icons/lib/fa/floppy-o';
import FaTrashO from 'react-icons/lib/fa/trash-o';
import noop from 'lodash/noop';
import has from 'lodash/has';
import bindAll from 'lodash/bindAll';
import isEmpty from 'lodash/isEmpty';
import RichTextEditor from 'react-rte';

import { QuestionType } from '../../shared/types';
import Button from '../../objects/Button';
import TextInput from '../../objects/TextInput';
import Textarea from '../../objects/Textarea';
import TextEditor from '../../objects/TextEditor';
import Select from '../../objects/Select';
import { Row, Field, Label, EditBox, Actions } from '../../objects/EditBox';

import './_admin-question.scss';

const questionOptionsTypes = [{
    label: 'Text',
    value: 'text'
}, {
    label: 'With Options',
    value: 'pick'
}];



const generateId = (prefix) => (
    (field) => `${prefix}-${field}`
);

export default class AdminQuestion extends PureComponent {

    static parseQuestion(question) {

        const body = question.body ?
            RichTextEditor.createValueFromString(question.body, 'html') :
            RichTextEditor.createEmptyValue();

        const options = (question.options || []).join('\n');

        return Object.assign({}, question, {
            options,
            body
        });
    }

    constructor(props) {
        super(props);

        const { question = {} } = props;

        this.state = AdminQuestion.parseQuestion(question);
        this.dirty = false;

        this.onTypeChange = this.onFieldChange('type');
        this.onAnswerChange = this.onFieldChange('answer');
        this.onOptionsChange = this.onFieldChange('options');

        bindAll(this, ['reset', 'save', 'onBodyChange', 'delete']);

    }

    componentWillReceiveProps(nextProps) {
        this.updateState(nextProps);
    }

    onBodyChange(newBody) {
        const { body } = this.state;
        this.setState({ body: newBody });
        if (newBody.toString('html') !== body.toString('html')) {
            this.dirty = true;
        }

    }

    onFieldChange(field) { //eslint-disable-line class-methods-use-this

        if (has(this.state, field) === false) {
            throw new Error(`Cannot find property "${field}" on the state`);
        }

        return function onFieldChangeHandler(e) {
            const value = this.state[field];
            const newValue = e.target.value;
            if (value !== newValue) {
                this.dirty = true;
                this.setState({
                    [field]: newValue
                });
            }
        }.bind(this);
    }

    validate() {
        const { options = '', body, type, answer, id, _uid } = this.state;
        const ret = { type, id, _uid };
        const hasBody = body.getEditorState().getCurrentContent().hasText();
        const answerString = typeof answer === 'string' ? answer.trim() : '';

        if (!hasBody || isEmpty(answerString)) {
            alert('Provide a question and an answer, please!'); //eslint-disable-line no-alert
            return false;
        }

        ret.body = body.toString('html').trim();
        ret.answer = answerString;

        if (type === 'pick') {
            ret.options = options.split('\n').map((str) => str.trim()).filter((str) => str.length > 0);
        } else {
            ret.options = [];
        }
        return ret;
    }

    delete(e) {
        e.preventDefault();

        //TODO: this is so ugly... maybe a better UI?
        if (confirm('Do you really want to delete this question?')) { //eslint-disable-line no-alert
            this.props.onDelete(this.state);
        }

    }

    save(e) {
        e.preventDefault();

        //format and validate values...
        const data = this.validate();

        if (data !== false) {
            this.props.onSave(data);
        }
    }

    reset() {
        this.updateState(this.props);
    }

    updateState(props) {
        const { question = {} } = props;
        const nextState = AdminQuestion.parseQuestion(question);

        this.setState(nextState);
        this.dirty = false;
    }

    render() {

        const { options, body, type, answer } = this.state;
        const { idx } = this.props;
        const questionId = generateId(`question-${idx}`);

        return (
            <EditBox className="c-admin-question" label={`Question ${idx + 1})`} id={`question-${idx}`} onSubmit={this.save}>
                <Row>
                    <Label id={questionId('type')}>
                        Type:
                    </Label>
                    <Field>
                        <Select
                            options={questionOptionsTypes}
                            value={type}
                            id={questionId('type')}
                            onChange={this.onTypeChange}
                        />
                    </Field>

                </Row>
                <Row>
                    <Label id={questionId('body')}>
                        Question:
                    </Label>
                    <Field>
                        <TextEditor
                            value={body}
                            id={questionId('body')}
                            onChange={this.onBodyChange}
                        />
                    </Field>
                </Row>
                {type === 'pick' && (<Row>
                    <Label id={questionId('options')}>
                        Options<span aria-label="One option per line">*</span> :
                    </Label>
                    <Field>
                        <Textarea
                            value={options}
                            id={questionId('options')}
                            required
                            onChange={this.onOptionsChange}
                        />
                        <p className="c-admin-question__hint" aria-hidden>* One option per line</p>
                    </Field>
                </Row>)}

                <Row>
                    <Label id={questionId('answer')}>
                        Answer:
                    </Label>
                    <Field>
                        <TextInput
                            value={answer}
                            required
                            id={questionId('answer')}
                            onChange={this.onAnswerChange}
                        />
                    </Field>
                </Row>
                <Actions>
                    <Button type="submit" disabled={this.dirty === false}><FaFloppyO /> Save</Button>
                    <Button disabled={this.dirty === false} className="o-button--secondary" onClick={this.reset}>Cancel</Button>
                    <Button className="o-button--action u-type--larger" title="Delete" onClick={this.delete}><FaTrashO /></Button>
                </Actions>
            </EditBox>
        );
    }

}

AdminQuestion.propTypes = {
    idx: PropTypes.number,
    onSave: PropTypes.func,
    onDelete: PropTypes.func,
    question: QuestionType
};

AdminQuestion.defaultProps = {
    onSave: noop,
    onDelete: noop
};