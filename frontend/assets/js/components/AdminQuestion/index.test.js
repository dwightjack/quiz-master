import React, { PureComponent } from 'react';
import { shallow } from 'enzyme';
import isPlainObject from 'lodash/isPlainObject';
import RichTextEditor from 'react-rte';

import { EditBox } from '../../objects/EditBox';
import { questions, emptyQuestion } from '../../__fixtures__/questions';
import AdminQuestion from './index';

/* eslint-env jest, jasmine */
describe('<AdminQuestion>', () => {


    describe('Generic tests', () => {

        it('should be a PureComponent instance', () => {
            const wrapper = shallow(<AdminQuestion question={emptyQuestion} />);
            expect(wrapper.instance()).toBeInstanceOf(PureComponent);
        });

    });


    describe('AdminQuestion.parseQuestion()', () => {

        it('should return an object', () => {
            const result = AdminQuestion.parseQuestion(questions[0]);
            expect(isPlainObject(result)).toBe(true);
        });


        it('should convert options to a multiline string', () => {

            const result = AdminQuestion.parseQuestion(questions[1]);
            const expected = questions[1].options.join('\n');

            expect(result.options).toBe(expected);
        });


        it('should convert `body` to a RichTextEditor value', () => {

            spyOn(RichTextEditor, 'createValueFromString').and.callThrough();
            AdminQuestion.parseQuestion(questions[0]);

            expect(RichTextEditor.createValueFromString).toHaveBeenCalledWith(
                questions[0].body,
                'html'
            );

        });

        it('should generate an empty RichTextEditor value on empty `body`', () => {

            spyOn(RichTextEditor, 'createEmptyValue').and.callThrough();
            AdminQuestion.parseQuestion(emptyQuestion);

            expect(RichTextEditor.createEmptyValue).toHaveBeenCalled();

        });


    });


    describe('constructor', () => {

        beforeEach(() => {
            spyOn(AdminQuestion.prototype, 'onFieldChange').and.callThrough();
        });

        it('should parse passed in `question` prop', () => {
            spyOn(AdminQuestion, 'parseQuestion').and.callThrough();
            shallow(<AdminQuestion question={emptyQuestion} />);
            expect(AdminQuestion.parseQuestion).toHaveBeenCalledWith(emptyQuestion);
        });


        it('should assign parsed question to the state', () => {
            const fakeState = Object.assign({}, emptyQuestion, { body: 'something else' });
            spyOn(AdminQuestion, 'parseQuestion').and.returnValue(fakeState);
            const wrapper = shallow(<AdminQuestion question={emptyQuestion} />);
            expect(wrapper.state()).toEqual(fakeState);
        });


        it('should set a `dirty` property on the instance', () => {
            const wrapper = shallow(<AdminQuestion question={emptyQuestion} />);
            expect(wrapper.instance().dirty).toBe(false);
        });


        it('should set `onTypeChange` function on the instance', () => {
            const wrapper = shallow(<AdminQuestion question={emptyQuestion} />);
            const instance = wrapper.instance();
            expect(AdminQuestion.prototype.onFieldChange).toHaveBeenCalledWith(
                'type'
            );
            expect(instance.onTypeChange).toEqual(expect.any(Function));
        });


        it('should set `onAnswerChange` function on the instance', () => {

            const wrapper = shallow(<AdminQuestion question={emptyQuestion} />);
            const instance = wrapper.instance();
            expect(
                AdminQuestion.prototype.onFieldChange
            ).toHaveBeenCalledWith('answer');
            expect(instance.onAnswerChange).toEqual(expect.any(Function));
        });


        it('should set `onOptionsChange` function on the instance', () => {

            const wrapper = shallow(<AdminQuestion question={emptyQuestion} />);
            const instance = wrapper.instance();
            expect(
                AdminQuestion.prototype.onFieldChange
            ).toHaveBeenCalledWith('options');
            expect(instance.onAnswerChange).toEqual(expect.any(Function));
        });

    });


    describe('.onBodyChange()', () => {

        class Body {

            constructor(str) {
                this.toString = jest.fn(() => str);
            }

        }

        let body;

        beforeEach(() => {
            body = new Body('my string');
        });

        it('should update state', () => {

            const wrapper = shallow(<AdminQuestion question={emptyQuestion} />);
            const instance = wrapper.instance();

            spyOn(instance, 'setState');
            instance.onBodyChange(body);
            expect(instance.setState).toHaveBeenCalled();
        });


        it('should compare new body and prev body calling their `toString` method', () => {
            const wrapper = shallow(<AdminQuestion question={emptyQuestion} />);
            const instance = wrapper.instance();
            const newBody = new Body('another string');

            wrapper.setState({ body });
            instance.onBodyChange(newBody);
            expect(newBody.toString).toHaveBeenCalledWith('html');
            expect(wrapper.state('body').toString).toHaveBeenCalledWith('html');
        });


        it('should set the `dirty` flag if body changes', () => {
            const wrapper = shallow(<AdminQuestion question={emptyQuestion} />);
            const instance = wrapper.instance();
            const newBody = new Body('another string');

            wrapper.setState({ body });
            instance.onBodyChange(newBody);
            expect(instance.dirty).toBe(true);
        });

    });


    describe('.onFieldChange()', () => {

        let wrapper;
        let instance;

        beforeEach(() => {
            wrapper = shallow(<AdminQuestion question={emptyQuestion} />);
            instance = wrapper.instance();
        });

        it('should throw on un-existent state properties', () => {

            expect(
                () => instance.onFieldChange('random-field')
            ).toThrow();

        });

        it('should return a function', () => {
            expect(
                instance.onFieldChange('type')
            ).toEqual(expect.any(Function));
        });


        it('should update the state branch', () => {

            const handler = instance.onFieldChange('answer');
            const event = { target: { value: '||new state||' } };

            handler(event);
            expect(wrapper.state('answer')).toBe(event.target.value);

        });


        it('should NOT update the state branch when values do not change', () => {

            const handler = instance.onFieldChange('answer');
            const event = { target: { value: 'my-value' } };

            wrapper.setState({ answer: 'my-value' });

            spyOn(instance, 'setState');

            handler(event);
            expect(instance.setState).not.toHaveBeenCalled();

        });

    });


    describe('.validate()', () => {

        let wrapper;
        let instance;

        beforeEach(() => {
            wrapper = shallow(<AdminQuestion question={emptyQuestion} />);
            instance = wrapper.instance();
        });


        it('should exit on empty body', () => {
            wrapper.setState({ answer: 'test' });
            expect(instance.validate()).toBe(false);
        });


        it('should exit on empty answer', () => {
            const newBody = RichTextEditor.createValueFromString('a string', 'html');
            wrapper.setState({ body: newBody });
            expect(instance.validate()).toBe(false);

            //also trims values
            wrapper.setState({ answer: ' ' });
            expect(instance.validate()).toBe(false);
        });


        it('should return a question object', () => {

            const body = RichTextEditor.createValueFromString('a string', 'html');

            const state = {
                body,
                answer: 'the answer',
                type: 'pick',
                options: 'one\ntwo',
                id: 1,
                _uid: '_string'
            };
            const expected = {
                body: body.toString('html').trim(),
                type: 'pick',
                answer: 'the answer',
                options: ['one', 'two'],
                id: 1,
                _uid: '_string'
            };
            wrapper.setState(state);

            expect(instance.validate()).toEqual(expected);
        });

    });


    describe('.delete()', () => {

        let wrapper;
        let instance;
        let onDelete;
        let event;

        beforeEach(() => {
            onDelete = jest.fn();
            event = { preventDefault: jest.fn() };
            wrapper = shallow(<AdminQuestion question={emptyQuestion} onDelete={onDelete} />);
            instance = wrapper.instance();
        });


        it('should call `preventDefault()` on the event', () => {
            instance.delete(event);
            expect(event.preventDefault).toHaveBeenCalled();
        });


        it('should call onDelete after use confirm', () => {
            spyOn(window, 'confirm').and.returnValue(true);
            instance.delete(event);
            expect(onDelete).toHaveBeenCalledWith(
                wrapper.state()
            );
        });


        it('should NOT call onDelete when user doesn\'t confirm', () => {
            spyOn(window, 'confirm').and.returnValue(false);
            instance.delete(event);
            expect(onDelete).not.toHaveBeenCalled();
        });

    });


    describe('.save()', () => {

        let wrapper;
        let instance;
        let onSave;
        let event;

        beforeEach(() => {
            onSave = jest.fn();
            event = { preventDefault: jest.fn() };
            wrapper = shallow(<AdminQuestion question={emptyQuestion} onSave={onSave} />);
            instance = wrapper.instance();
        });


        it('should call `preventDefault()` on the event', () => {
            instance.save(event);
            expect(event.preventDefault).toHaveBeenCalled();
        });


        it('should call onSave when data are validated', () => {
            const data = { id: 2 };
            spyOn(instance, 'validate').and.returnValue(data);
            instance.save(event);
            expect(onSave).toHaveBeenCalledWith(
                data
            );
        });


        it('should NOT call onSave when .validate() returns `false`', () => {
            spyOn(instance, 'validate').and.returnValue(false);
            instance.save(event);
            expect(onSave).not.toHaveBeenCalled();
        });

    });


    describe('.updateState()', () => {

        let wrapper;
        let instance;

        beforeEach(() => {
            wrapper = shallow(<AdminQuestion question={emptyQuestion} />);
            instance = wrapper.instance();
        });


        it('should call `AdminQuestion.parseQuestion()` with the new question', () => {
            spyOn(AdminQuestion, 'parseQuestion');
            const question = { id: 10 };

            instance.updateState({ question });
            expect(AdminQuestion.parseQuestion).toHaveBeenCalledWith(question);
        });


        it('should call `setState` with the parsed question', () => {
            const expected = { prop: 'value' };
            spyOn(AdminQuestion, 'parseQuestion').and.returnValue(expected);
            spyOn(instance, 'setState');
            const question = { id: 10 };

            instance.updateState({ question });
            expect(instance.setState).toHaveBeenCalledWith(expected);
        });


        it('should set `dirty` to false', () => {
            const expected = { prop: 'value' };
            spyOn(AdminQuestion, 'parseQuestion').and.returnValue(expected);
            spyOn(instance, 'setState');
            const question = { id: 10 };

            instance.updateState({ question });
            expect(instance.dirty).toBe(false);
        });

    });


    describe('.reset()', () => {

        let wrapper;
        let instance;

        beforeEach(() => {
            wrapper = shallow(<AdminQuestion question={emptyQuestion} />);
            instance = wrapper.instance();
        });

        it('should call`.updateState()` with current props', () => {

            spyOn(instance, 'updateState');

            instance.reset();
            expect(instance.updateState).toHaveBeenCalledWith(instance.props);

        });
    });


    describe('.componentWillReceiveProps()', () => {

        let wrapper;
        let instance;

        beforeEach(() => {
            wrapper = shallow(<AdminQuestion question={emptyQuestion} />);
            instance = wrapper.instance();
        });

        it('should call`.updateState()` with current props', () => {

            spyOn(instance, 'updateState');
            const question = { id: 10 };
            const props = { question };

            instance.componentWillReceiveProps(props);
            expect(instance.updateState).toHaveBeenCalledWith(props);

        });
    });


    describe('.render()', () => {

        let wrapper;
        let instance;

        beforeEach(() => {
            wrapper = shallow(<AdminQuestion question={emptyQuestion} />);
            instance = wrapper.instance();
        });

        it('should render and <EditBox> element', () => {
            expect(wrapper.is(EditBox)).toBe(true);
        });

        it('should call `.save()` on <EditBox> submit', () => {
            expect(wrapper.prop('onSubmit')).toBe(instance.save);
        });


        it('should render 3 <Row> on question.type === "text" ', () => {
            expect(wrapper.find('Row').length).toBe(3);
        });


        it('should render 4 <Row> on question.type === "pick" ', () => {
            wrapper.setProps({
                question: Object.assign({}, emptyQuestion, { type: 'pick' })
            });
            expect(wrapper.find('Row').length).toBe(4);
        });


        it('should set a <Select> for type field', () => {
            const field = wrapper.find('Select');
            expect(field.length).toBe(1);
            expect(field.prop('value')).toBe(wrapper.state('type'));
            expect(field.prop('onChange')).toBe(instance.onTypeChange);
        });


        it('should set a <TextEditor> for body field', () => {
            const field = wrapper.find('TextEditor');
            expect(field.length).toBe(1);
            expect(field.prop('value')).toBe(wrapper.state('body'));
            expect(field.prop('onChange')).toBe(instance.onBodyChange);
        });


        it('should set a <TextInput> for answer field', () => {
            const field = wrapper.find('TextInput');
            expect(field.length).toBe(1);
            expect(field.prop('value')).toBe(wrapper.state('answer'));
            expect(field.prop('onChange')).toBe(instance.onAnswerChange);
        });


        it('should set a <Textarea> for options field', () => {
            wrapper.setProps({
                question: Object.assign({}, emptyQuestion, { type: 'pick' })
            });

            const field = wrapper.find('Textarea');
            expect(field.length).toBe(1);
            expect(field.prop('value')).toBe(wrapper.state('options'));
            expect(field.prop('onChange')).toBe(instance.onOptionsChange);
        });


        it('should render an <Actions> element', () => {
            expect(wrapper.find('Actions').length).toBe(1);
        });


        it('should contain a save button. Enabled on dirty=true', () => {
            const button = wrapper.find('Button').filter('[type="submit"]');
            expect(button.length).toBe(1);
            expect(button.prop('disabled')).toBe(true);

            //change a field
            instance.onTypeChange({ target: { value: 'pick' } });
            expect(wrapper.find('Button').filter('[type="submit"]').prop('disabled')).toBe(false);

        });


        it('should contain a reset button. Enabled on dirty=true', () => {
            const button = wrapper.find('.o-button--secondary');
            expect(button.length).toBe(1);
            expect(button.prop('disabled')).toBe(true);
            expect(button.prop('onClick')).toBe(instance.reset);

            //change a field
            instance.onTypeChange({ target: { value: 'pick' } });
            expect(wrapper.find('.o-button--secondary').prop('disabled')).toBe(false);

        });


        it('should contain a delete action button', () => {
            const button = wrapper.find('.o-button--action');
            expect(button.length).toBe(1);
            expect(button.prop('title')).toBe('Delete');
            expect(button.prop('onClick')).toBe(instance.delete);

        });


    });

});