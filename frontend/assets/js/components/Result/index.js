import React, { PureComponent, PropTypes } from 'react';
import classnames from 'classnames';
import isEmpty from 'lodash/isEmpty';
import padStart from 'lodash/padStart';
import FaCheck from 'react-icons/lib/fa/check';
import FaClose from 'react-icons/lib/fa/close';

import './_result.scss';

export default class Result extends PureComponent {

    constructor(props) {
        super(props);

        this.qIdx = this.createqIdx(props.idx);
    }

    componentWillReceiveProps(nextProps) {
        const { idx } = this.props;

        if (nextProps.idx !== idx) {
            this.qIdx = this.createqIdx(nextProps.idx);
        }
    }

    createqIdx(idx) { //eslint-disable-line class-methods-use-this

        if (Number.isInteger(idx) === false) {
            throw new TypeError(`idx prop should be an integer: ${idx}`);
        }

        return padStart(idx + 1, 2, '0');
    }

    buildAnswerCheck() {

        const { data = {} } = this.props;

        if (isEmpty(data)) {
            return null;
        }

        const { answer, userAnswer, correct } = data;
        const correctIco = correct ? <FaCheck /> : <FaClose />;

        return (
            <div className="c-result__check">
                {userAnswer && <p className="c-result__user"><span className="u-visuallyhidden">Your answer: </span>{correctIco}{userAnswer}</p>}
                {correct === false && <p className="c-result__answer"><span className="u-visuallyhidden">Correct answer: </span><FaCheck />{answer}</p>}
            </div>
        );
    }

    render() {

        const { className, data = {} } = this.props;
        const { question, userAnswer, correct } = data;
        const status = userAnswer ? (correct ? 'correct' : 'wrong') : 'missed'; //eslint-disable-line no-nested-ternary
        const answerCheck = this.buildAnswerCheck();

        return (
            <article className={classnames('c-result', className, `is-${status}`)}>
                <header className="c-result__header">
                    <p className="c-result__id" data-qid={this.qIdx}>
                        <span className="u-visuallyhidden">Question n. {this.qIdx}: {status}</span>
                    </p>
                    {/*eslint-disable react/no-danger */}
                    <div className="c-result__question" dangerouslySetInnerHTML={{ __html: question }} />
                    {/*eslint-enable react/no-danger */}
                </header>
                {answerCheck}
            </article>
        );
    }

}

Result.propTypes = {
    idx: PropTypes.number,
    className: PropTypes.string,
    data: PropTypes.shape({
        qid: PropTypes.number,
        question: PropTypes.string,
        answer: PropTypes.string,
        userAnswer: PropTypes.string,
        correct: PropTypes.bool
    })
};

Result.defaultProps = {
    idx: 0,
    data: {}
};