import React, { PureComponent } from 'react';
import { shallow } from 'enzyme';

import FaCheck from 'react-icons/lib/fa/check';
import FaClose from 'react-icons/lib/fa/close';

import Result from './index';

/* eslint-env jest, jasmine */
describe('<Result>', () => {

    const successData = { question: '<p>question body</p>', userAnswer: 'yes', answer: 'yes', correct: true };
    const errorData = { question: '<p>question body</p>', userAnswer: 'no', answer: 'yes', correct: false };
    const missedData = { question: '<p>question body</p>', answer: 'yes', correct: false };


    describe('Generic tests', () => {

        it('should be a PureComponent instance', () => {
            const result = shallow(<Result />);
            expect(result.instance()).toBeInstanceOf(PureComponent);
        });

    });

    describe('`className` prop', () => {

        it('should add a custom className on the root element', () => {

            const result = shallow(<Result />);
            expect(result.prop('className')).toContain('c-result');

            result.setProps({ className: 'my-class' });
            expect(result.prop('className')).toContain('c-result my-class');

        });

    });


    describe('.createqIdx()', () => {

        let result;
        let instance;


        beforeEach(() => {
            result = shallow(<Result idx={2} />);
            instance = result.instance();
        });


        it('should allow just numeric idx values', () => {

            expect(() => {
                instance.createqIdx('string');
            }).toThrowError(TypeError);

        });


        it('should add +1 to passed in number and convert it to string', () => {

            expect(instance.createqIdx(10)).toEqual(expect.any(String));
            expect(instance.createqIdx(10)).toBe('11');

        });


        it('should left pad `idx` just when it is less than 10', () => {

            expect(instance.createqIdx(7)).toBe('08');
            expect(instance.createqIdx(10)).toBe('11');

        });


    });

    describe('.buildAnswerCheck()', () => {

        it('should return `null` if `data` prop is empty', () => {
            const instance = shallow(<Result data={{}} />).instance();
            expect(instance.buildAnswerCheck()).toBe(null);
        });


        it('should return a `div` root element with class c-result__check', () => {
            const instance = shallow(<Result data={successData} />).instance();
            expect(shallow(instance.buildAnswerCheck()).is('div.c-result__check')).toBe(true);
        });


        it('should show user answer if provided', () => {

            const instance = shallow(<Result data={successData} />).instance();

            expect(
                shallow(instance.buildAnswerCheck()).find('p.c-result__user').length
            ).toBe(1);

            const missedInstance = shallow(<Result data={missedData} />).instance();

            expect(
                shallow(missedInstance.buildAnswerCheck()).find('p.c-result__user').length
            ).toBe(0);

        });


        it('should provide a visual icon feedback on success', () => {

            const successInstance = shallow(<Result data={successData} />).instance();

            expect(
                shallow(successInstance.buildAnswerCheck()).find('p.c-result__user').find(FaCheck).length
            ).toBe(1);

        });


        it('should provide a visual icon feedback on error', () => {

            const errorInstance = shallow(<Result data={errorData} />).instance();

            expect(
                shallow(errorInstance.buildAnswerCheck()).find('p.c-result__user').find(FaClose).length
            ).toBe(1);

        });


        it('should provide the correct answer when `data.correct` is false', () => {

            const successInstance = shallow(<Result data={successData} />).instance();
            const errorInstance = shallow(<Result data={errorData} />).instance();

            const check = (inst) => shallow(inst.buildAnswerCheck()).find('.c-result__answer');

            expect(
                check(successInstance).length
            ).toBe(0);

            expect(
                check(errorInstance).length
            ).toBe(1);

            expect(
                check(errorInstance).text()
            ).toContain(errorData.answer);

        });

    });


    describe('Lifecycle', () => {

        let result;
        let instance;

        beforeEach(() => {
            result = shallow(<Result idx={2} data={successData} />);
            instance = result.instance();
        });

        it('should create .qIdx property on constructor by calling .createqIdx()', () => {

            spyOn(Result.prototype, 'createqIdx').and.callThrough();

            result = shallow(<Result idx={2} data={successData} />);

            expect(instance.qIdx).toEqual(expect.any(String));
            expect(instance.createqIdx).toHaveBeenCalled();
        });


        it('should update .qIdx on new props by calling .createqIdx()', () => {

            spyOn(instance, 'createqIdx').and.callThrough();
            const expected = instance.qIdx;
            result.setProps({ idx: 20 });
            expect(instance.qIdx).not.toBe(expected);
            expect(instance.createqIdx).toHaveBeenCalled();

        });

    });


    describe('.render()', () => {

        let result;
        let instance;

        beforeEach(() => {
            result = shallow(<Result idx={2} data={successData} />);
            instance = result.instance();
        });


        it('should render an article with a default class', () => {
            expect(result.is('article.c-result')).toBe(true);
        });


        it('should add passed in className', () => {
            result.setProps({ className: 'my-class' });
            expect(result.is('article.c-result.my-class')).toBe(true);
        });


        it('should set a `is-correct` state class on success', () => {
            result.setProps({ data: successData });
            expect(result.is('.is-correct')).toBe(true);
        });


        it('should set a `is-wrong` state class on error', () => {
            result.setProps({ data: errorData });
            expect(result.is('.is-wrong')).toBe(true);
        });


        it('should set a `is-missed` state class on no answer', () => {
            result.setProps({ data: missedData });
            expect(result.is('.is-missed')).toBe(true);
        });


        it('should place a `data-qid` attribute on `.c-result__id`', () => {
            const expected = instance.qIdx;
            expect(result.find('.c-result__id').prop('data-qid')).toBe(expected);
        });


        it('should show the question body as HTML', () => {
            const expected = successData.question;
            expect(result.find('.c-result__question').html()).toContain(expected);
        });


        it('should contain the answer check', () => {
            expect(result.find('.c-result__check').length).toBe(1);
        });

    });

});