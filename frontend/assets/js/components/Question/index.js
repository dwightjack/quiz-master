import React, { PureComponent, PropTypes } from 'react';
import bindAll from 'lodash/bindAll';
import padStart from 'lodash/padStart';

import { QuestionType } from '../../shared/types';
import TextInput from '../../objects/TextInput';
import Radio from '../../objects/Radio';

import './_question.scss';

/* eslint-disable react/no-array-index-key */

export default class Question extends PureComponent {

    static getStateFromProps(props) {
        const { question = {}, answer } = props;
        return Object.assign({
            answer: '',
            qid: (question.id || null)
        }, answer || {});
    }

    constructor(props) {
        super(props);

        this.state = Question.getStateFromProps(props);

        bindAll(this, ['onAnswerChange']);
    }

    componentWillReceiveProps(nextProps) {

        const nextState = Question.getStateFromProps(nextProps);

        this.setState(nextState);
    }

    componentWillUnmount() {
        this.props.onChange(this.state);
    }

    onAnswerChange({ target }) {
        const answer = target.value.trim();
        this.setState({ answer });

    }

    buildOptionsList(options, questionId, answer) {
        const answerId = `answer-${questionId}`;
        return (
            <div className="c-question__radiogroup" role="radiogroup" aria-labelledby={`question-label-${questionId}`}>
                {options.map((option, i) => (
                    <Radio
                        label={option}
                        id={`${answerId}-${i}`}
                        key={`${answerId}-${i}`}
                        name={answerId}
                        onChange={this.onAnswerChange}
                        checked={option === answer}
                        value={option}
                    />
                ))}
            </div>
        );

    }

    render() {

        const { question, idx } = this.props;
        const { answer } = this.state;
        const { type, body, id, options } = question;

        const questionNum = (idx + 1);
        const answerId = `answer-${id}`;

        const questionField = type === 'pick' ?
            this.buildOptionsList(options, id, answer) :
            <TextInput aria-labelledby={`question-label-${id}`} onChange={this.onAnswerChange} name={answerId} id={answerId} value={answer} />;

        return (
            <fieldset className="c-question" id={`question-${id}`}>
                <legend className="c-question__num" aria-label={`Question number ${questionNum}`}>{ padStart(questionNum, 2, '0') }</legend>
                {/*eslint-disable react/no-danger */}
                <div id={`question-label-${id}`} className="c-question__body" dangerouslySetInnerHTML={{ __html: body }} />
                {/*eslint-enable react/no-danger */}
                <div className="c-question__answer">
                    {questionField}
                </div>
            </fieldset>
        );
    }

}

Question.propTypes = {
    idx: PropTypes.number,
    question: QuestionType,
    answer: PropTypes.shape({ //eslint-disable-line react/no-unused-prop-types
        qid: PropTypes.number,
        answer: PropTypes.string
    }),
    onChange: PropTypes.func
};
Question.defaultProps = {};