import React, { PureComponent } from 'react';
import { shallow } from 'enzyme';
import isPlainObject from 'lodash/isPlainObject';

import TextInput from '../../objects/TextInput';
import Radio from '../../objects/Radio';

import Question from './index';

/* eslint-env jest, jasmine */
describe('<Question>', () => {

    const textQuestion = {
        body: '<p>question-body</p>',
        type: 'text',
        id: 10,
        options: []
    };

    const pickQuestion = {
        body: '<p>question-body</p>',
        type: 'pick',
        id: 10,
        options: ['1', '2', '3']
    };

    const previousAnswer = {
        qid: 10,
        answer: 'old text'
    };


    describe('Generic tests', () => {

        it('should be a PureComponent instance', () => {
            const result = shallow(<Question question={textQuestion} />);
            expect(result.instance()).toBeInstanceOf(PureComponent);
        });

    });

    describe('Question.getStateFromProps', () => {

        it('should return an object', () => {
            expect(isPlainObject(Question.getStateFromProps({}))).toBe(true);
        });

        it('should merge passed in object with defaults', () => {
            expect(
                Question.getStateFromProps({ question: { id: 10 } })
            ).toMatchObject({ answer: '', qid: 10 });

            expect(
                Question.getStateFromProps({ answer: { answer: 'test', qid: null } })
            ).toMatchObject({ answer: 'test', qid: null });
        });

    });


    describe('.render()', () => {

        it('should render a `fieldset.c-question` element', () => {
            const question = shallow(<Question question={textQuestion} />);
            expect(question.is('fieldset.c-question')).toBe(true);
        });


        it('should render a `TextInput` element', () => {
            const question = shallow(<Question question={textQuestion} />);
            expect(question.find(TextInput).length).toBe(1);
        });


        it('should render `Radio` elements on `pick` type prop', () => {
            const question = shallow(<Question question={pickQuestion} />);
            expect(question.find(Radio).length).toBe(pickQuestion.options.length);
        });

        it('should render the question as HTML', () => {
            const question = shallow(<Question question={textQuestion} />);
            expect(question.find('.c-question__body').html()).toContain(textQuestion.body);
        });

    });

    describe('Lifecycle', () => {

        it('should generate a state based on passed-in props', () => {

            const expected = Question.getStateFromProps({ question: textQuestion, answer: previousAnswer });

            spyOn(Question, 'getStateFromProps').and.callThrough();

            const question = shallow(<Question question={textQuestion} answer={previousAnswer} />);
            expect(question.state()).toEqual(expected);
            expect(Question.getStateFromProps).toHaveBeenCalled();
        });

        it('should update a state based on passed-in props', () => {
            const question = shallow(<Question question={textQuestion} />);
            question.setProps({ answer: previousAnswer });

            const expected = Question.getStateFromProps({ question: textQuestion, answer: previousAnswer });
            expect(question.state()).toEqual(expected);
        });


        it('should update the state on text input change', () => {
            const question = shallow(<Question question={textQuestion} />);

            expect(question.state('answer')).toBe('');

            question.find(TextInput).simulate('change', { target: { value: 'my-text' } });

            expect(question.state('answer')).toBe('my-text');
        });


        it('should update the state on radio input change', () => {
            const question = shallow(<Question question={pickQuestion} />);

            expect(question.state('answer')).toBe('');

            question.find(Radio).at(0).simulate('change', { target: { value: '0' } });

            expect(question.state('answer')).toBe('0');
        });


        it('should call the provided on change prop on unmount passing the state', () => {

            const spy = jest.fn();
            const question = shallow(<Question question={pickQuestion} onChange={spy} />);

            const expected = question.state();
            question.unmount();
            expect(spy).toHaveBeenCalledWith(expected);
        });


    });


});