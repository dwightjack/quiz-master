//global UI state management...
export const UI_LOADING = 'ui/LOADING';

export const loadingAction = (toggle) => ({
    type: UI_LOADING,
    payload: toggle
});

export const loadingReducer = (state = false, { type, payload }) => {
    if (type === UI_LOADING) {
        return payload;
    }
    return state;
};