/* eslint-env jest */

const api = jest.genMockFromModule('../api');

api.post = jest.fn(() => Promise.resolve());

module.exports = api;