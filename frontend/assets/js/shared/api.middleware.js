/**
 * Asyc API call middleware for Redux
 */



/**
 * Middleware constructor
 * @param {function} dispatch - Redux store dispatcher
 * @param {function} getState - Redux store state getter
 * @returns {function}
 */
function callAPIMiddleware({ dispatch, getState }) {
    return (next) => { //eslint-disable-line arrow-body-style
        return (action) => {
            const {
                types,
                callAPI,
                shouldCallAPI = () => true,
                payload = {}
            } = action;

            if (!types) {
                // Normal action: pass it on
                return next(action);
            }

            if (
                !Array.isArray(types) ||
                types.length !== 3 ||
                !types.every(type => typeof type === 'string')
            ) {
                throw new Error('Expected an array of three string types.');
            }

            if (typeof callAPI !== 'function') {
                throw new Error('Expected fetch to be a function.');
            }

            if (shouldCallAPI(getState()) === false) {
                return false;
            }

            const [requestType, successType, failureType] = types;

            dispatch({
                type: requestType,
                payload
            });

            return callAPI().then(
                (response) => dispatch({
                    type: successType,
                    payload: Array.isArray(response) ? response : Object.assign({}, payload, response)
                }),
                (error) => dispatch({
                    type: failureType,
                    error,
                    payload
                })
            );
        };
    };
}


export default callAPIMiddleware;