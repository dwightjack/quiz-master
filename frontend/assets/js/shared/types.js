import { PropTypes } from 'react';


export const QuestionType = PropTypes.shape({
    id: PropTypes.number,
    _uid: PropTypes.string,
    type: PropTypes.oneOf(['text', 'pick']).isRequired,
    body: PropTypes.string.isRequired,
    answer: PropTypes.string,
    options: PropTypes.arrayOf(
        PropTypes.string
    )
});

export const AnswerType = PropTypes.shape({
    qid: PropTypes.number,
    answer: PropTypes.string.isRequired
});