import axios from 'axios';
import { compose } from 'redux';

import {
    API_BASE
} from './constants';

const errorHandler = (error) => {
    if (error.response) {
        // The request was made, but the server responded with a status code
        // that falls out of the range of 2xx
        console.warn(error.response.data); //eslint-disable-line no-console
        console.warn(error.response.status); //eslint-disable-line no-console
        console.warn(error.response.headers); //eslint-disable-line no-console
    } else {
        // Something happened in setting up the request that triggered an Error
        console.warn('Error', error.message); //eslint-disable-line no-console
    }
    console.error(error); //eslint-disable-line no-console
    return Promise.reject(error);
};


const instance = axios.create({
    baseURL: API_BASE
});

instance.interceptors.response.use(
    ({ data }) => {
        if (!data) {
            throw new Error('API fetch error');
        } else {
            return Promise.resolve(data.data);
        }
    },
    errorHandler
);


const api = {

    loadTick: 0,

    tickUp() {
        this.loadTick += 1;
        if (this.loadTick === 1) {
            this.dispatch(true);
        }
    },

    tickDown() {
        this.loadTick = Math.max(0, this.loadTick - 1);
        if (this.loadTick === 0) {
            this.dispatch(false);
        }
    },

    connect(store, loadAction) {

        this.dispatch = compose(store.dispatch, loadAction);

        instance.interceptors.request.use((config) => {
            this.tickUp();
            return config;
        });

        // Add a response interceptor
        instance.interceptors.response.use((response) => {
            this.tickDown();
            return response;
        }, (error) => {
            this.tickDown();
            return Promise.reject(error);
        });
    }
};

['get', 'post', 'delete', 'put', 'patch'].forEach((method) => {
    api[method] = instance[method].bind(instance);
});

export default api;

export {
    instance as axiosInstance
};