export const API_BASE = '/api/v1/';
export const API_ENDPOINT_QUESTIONS = 'questions/';
export const API_ENDPOINT_ANSWERS = 'answers/';