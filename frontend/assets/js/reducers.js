import { combineReducers } from 'redux';

import admin from './containers/Admin/reducers';
import {
    questionsReducer as questions,
    answersReducer as answers
} from './containers/Quiz/reducers';
import { loadingReducer as loading } from './shared/ui';


export default combineReducers({
    admin,
    questions,
    answers,
    loading
});