import thunkMiddleware from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import createLogger from 'redux-logger';

import callAPIMiddleware from './shared/api.middleware';
import reducer from './reducers';

const middlewares = [
    thunkMiddleware,
    callAPIMiddleware
];

if (__PRODUCTION__ === false) {
    middlewares.push(createLogger({ collapsed: true }));
}

export default function configureStore(initialState = {}) {
    const store = createStore(
        reducer,
        initialState,
        applyMiddleware(
            ...middlewares
        )
    );

    if (__PRODUCTION__ === false) {

        if (module.hot) {
            module.hot.accept('./reducers', () =>
                store.replaceReducer(require('./reducers')) //eslint-disable-line global-require
            );
        }

    }

    return store;
}