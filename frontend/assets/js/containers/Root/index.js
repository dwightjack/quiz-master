import React, { Component } from 'react';
import Router from 'react-router/lib/Router';
import browserHistory from 'react-router/lib/browserHistory';
import Route from 'react-router/lib/Route';
import IndexRoute from 'react-router/lib/IndexRoute';
import { Provider } from 'react-redux';
import { ConnectedApp as App } from '../App';
import { ConnectedAdmin as Admin } from '../Admin';
import { ConnectedQuiz as Quiz } from '../Quiz';
import { connectedResults as Results } from '../Results';

export default class Root extends Component {

    render() {
        const { store } = this.props; //eslint-disable-line react/prop-types

        return (
            <Provider store={store}>
                {/* HMR fix. @see https://github.com/reactjs/react-router-redux/issues/179#issuecomment-275576250 */}
                <Router key={__PRODUCTION__ ? null : Math.random()} history={browserHistory}>
                    <Route path="/" component={App}>
                        <IndexRoute component={Quiz} />
                        <Route path="admin" component={Admin} />
                        <Route path="results" component={Results} />
                    </Route>
                </Router>
            </Provider>
        );
    }

}