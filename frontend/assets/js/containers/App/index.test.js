import React from 'react';
import { shallow } from 'enzyme';

import App from './index';
import Loader from '../../objects/Loader';

/* eslint-env jest */
describe('<App>', () => {

    it('should allow for children', () => {
        const app = shallow(<App><p>child</p></App>);
        expect(app.find('p').length).toBe(1);
    });

    it('should render a loader', () => {
        const app = shallow(<App />);
        expect(app.find(Loader).length).toBe(1);
    });


    it('should receive a `loading` prop passed to the loader', () => {
        const app = shallow(<App loading />);
        expect(app.find(Loader).prop('visible')).toBe(true);
    });

});