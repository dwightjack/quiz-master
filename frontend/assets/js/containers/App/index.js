import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Loader from '../../objects/Loader';

import './_app.scss';

export default class App extends Component {


    render() {

        const { children, loading } = this.props;

        return (
            <div id="container">
                {children}
                <Loader visible={loading} />
            </div>
        );
    }

}

App.propTypes = {
    children: PropTypes.node,
    loading: PropTypes.bool
};

const mapStateToProps = ({ loading }) => ({ loading });

export const ConnectedApp = connect(
    mapStateToProps
)(App);