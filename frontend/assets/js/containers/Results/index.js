import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Link from 'react-router/lib/Link';
import FaChevronLeft from 'react-icons/lib/fa/chevron-left';

import api from '../../shared/api';
import { AnswerType } from '../../shared/types';
import { API_ENDPOINT_ANSWERS } from '../../shared/constants';

import Result from '../../components/Result';
import Well from '../../objects/Well';

import './_results.scss';


export default class Results extends Component {

    constructor(props) {
        super(props);

        //Not storing in redux the results, since they are not useful elsewhere...
        //loading them async in state for this session only
        this.state = {
            results: [],
            error: null
        };
    }

    componentDidMount() {
        this.submitAnswers();
    }

    submitAnswers() {
        const { answers = [] } = this.props;

        if (answers.length > 0) {
            return api.post(API_ENDPOINT_ANSWERS, answers).then((results) => {
                this.setState({ results });
            }).catch((error) => {
                this.setState({ error });
            });
        }
        return null;
    }


    buildResultsList() {
        const { results } = this.state;

        if (results.length === 0) {
            return null;
        }

        return (
            <ol className="c-results__list">
                {results.map((result, i) => (
                    <li className="c-results__item" key={result.qid}>
                        <Result data={result} idx={i} />
                    </li>
                ))}
            </ol>
        );
    }

    renderTitle() {
        const { results } = this.state;

        if (results.length === 0) {
            return null;
        }

        const correctTotal = results.filter(({ correct }) => correct === true).length;

        return (
            <h2 className="c-results__summary">
                Results: <strong>{correctTotal}</strong> out of {results.length}
            </h2>
        );
    }

    renderError() {
        const { error } = this.state;
        return (
            <Well className="o-well--error">
                <h3 className="h5">Something went wrong submitting your answers...</h3>
                <p>Please report to the sys admin</p>
                <pre><code>{error.toString()}</code></pre>
            </Well>
        );
    }

    render() {

        const { error } = this.state;
        const { answers } = this.props;

        const title = this.renderTitle();
        const resultList = this.buildResultsList();

        return (
            <div className="c-results">
                <h1 className="h1 c-results__title" id="main-title">Results</h1>
                <main id="main" role="main" className="c-results__body">

                    {error && this.renderError()}


                    {answers.length === 0 && (<Well className="o-well--error">
                        <p>Looks like you didn&rsquo;t take the quiz...</p>
                    </Well>)}

                    {title}
                    {resultList}

                    <footer className="c-results__actions">
                        <Link className="o-button o-button--primary" to="/">
                            {answers.length > 0 ? <span><FaChevronLeft /> Take the quiz again</span> : <span>Take the quiz</span>}
                        </Link>
                    </footer>
                </main>
            </div>
        );
    }

}

Results.propTypes = {
    answers: PropTypes.arrayOf(AnswerType)
};

Results.defaultProps = {
    answers: []
};

const mapStateToProps = ({ answers }) => ({ answers });

export const connectedResults = connect(
    mapStateToProps
)(Results);