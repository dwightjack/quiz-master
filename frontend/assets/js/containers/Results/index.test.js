import React from 'react';
import { shallow } from 'enzyme';
import moxios from 'moxios';
import { API_ENDPOINT_ANSWERS, API_BASE } from '../../shared/constants';
import api, { axiosInstance } from '../../shared/api';
import Results from './index';

/* eslint-env jest, jasmine */
describe('<Results>', () => {

    const answers = [{ qid: 1, answer: 'the answer' }];

    const results = [{
        qid: 1,
        question: '<p>question body</p>',
        userAnswer: 'yes',
        answer: 'yes',
        correct: true
    }, {
        qid: 2,
        question: '<p>question body</p>',
        answer: 'yes',
        correct: false
    }, {
        qid: 3,
        question: '<p>question body</p>',
        userAnswer: 'yes',
        answer: 'yes',
        correct: false
    }];

    describe('constructor', () => {

        it('should populate state with a result array and `null` error', () => {
            const wrapper = shallow(<Results />);
            expect(wrapper.state('results')).toEqual(expect.any(Array));
            expect(wrapper.state('error')).toBe(null);
        });

    });


    describe('.submitAnswers()', () => {

        beforeEach(() => {
            moxios.install(axiosInstance);



        });

        afterEach(() => {
            // import and pass your custom axios instance to this method
            moxios.uninstall(axiosInstance);
        });

        it('should submit answers by API and call setState on success', (done) => {

            const wrapper = shallow(<Results answers={answers} />);
            const instance = wrapper.instance();

            spyOn(api, 'post').and.callThrough();

            moxios.stubRequest(API_BASE + API_ENDPOINT_ANSWERS, {
                status: 200,
                response: { data: results }
            });

            instance.submitAnswers();

            expect(api.post).toHaveBeenCalledWith(
                API_ENDPOINT_ANSWERS,
                answers
            );

            moxios.wait(() => {
                expect(wrapper.state('results')).toEqual(results);
                done();
            });
        });

        it('should submit answers by API and call setState on error', (done) => {

            const wrapper = shallow(<Results answers={answers} />);
            const instance = wrapper.instance();

            spyOn(api, 'post').and.callThrough();

            moxios.stubRequest(API_BASE + API_ENDPOINT_ANSWERS, {
                status: 400,
                response: { message: 'problem' }
            });

            instance.submitAnswers();

            expect(api.post).toHaveBeenCalledWith(
                API_ENDPOINT_ANSWERS,
                answers
            );

            moxios.wait(() => {
                expect(wrapper.state('error')).not.toBe(null);
                done();
            });

        });
    });


    describe('.buildResultsList()', () => {

        it('should return a list of <Result>s', () => {

            const wrapper = shallow(<Results />);
            wrapper.setState({ results });

            const list = shallow(
                wrapper.instance().buildResultsList()
            );
            expect(list.find('Result').length).toBe(results.length);

        });


        it('should return null when results list is empy', () => {
            const wrapper = shallow(<Results />);
            const list = wrapper.instance().buildResultsList();
            expect(list).toBe(null);
        });

    });


    describe('.renderTitle()', () => {

        it('should return a summary with matched and total', () => {

            const wrapper = shallow(<Results />);
            const expectedMatched = results.filter(({ correct }) => correct).length;
            wrapper.setState({ results });

            const title = shallow(
                wrapper.instance().renderTitle()
            );

            expect(title.is('.c-results__summary')).toBe(true);
            expect(title.find('strong').text()).toBe(expectedMatched.toString());
            expect(title.html()).toContain(results.length.toString());

        });


        it('should return null when results list is empy', () => {
            const wrapper = shallow(<Results />);
            const list = wrapper.instance().buildResultsList();
            expect(list).toBe(null);
        });

    });


    describe('.renderError()', () => {

        it('should return an error well', () => {

            const wrapper = shallow(<Results />);
            wrapper.setState({ error: '!!err!!' });

            const error = shallow(
                wrapper.instance().renderError()
            );

            expect(error.is('.o-well--error')).toBe(true);
            expect(error.html()).toContain('!!err!!');

        });

    });


    describe('.render()', () => {

        it('should render a `.c-results` root element', () => {
            const wrapper = shallow(<Results />);
            expect(wrapper.is('.c-results')).toBe(true);
        });


        it('should render an error well on error', () => {
            const wrapper = shallow(<Results />);
            const instance = wrapper.instance();

            spyOn(instance, 'renderError');
            wrapper.setState({ error: 'err' });

            expect(instance.renderError).toHaveBeenCalled();
        });


        it('should render a title and the results list', () => {
            const wrapper = shallow(<Results />);
            const instance = wrapper.instance();

            spyOn(instance, 'renderTitle');
            spyOn(instance, 'buildResultsList');
            wrapper.setState({ results });

            expect(instance.renderTitle).toHaveBeenCalled();
            expect(instance.buildResultsList).toHaveBeenCalled();

        });
    });

    describe('.componentDidMount()', () => {

        it('should submit answers on mount', () => {

            const wrapper = shallow(<Results answers={answers} />);
            const instance = wrapper.instance();

            spyOn(instance, 'submitAnswers');

            instance.componentDidMount();

            expect(instance.submitAnswers).toHaveBeenCalled();

        });
    });

});