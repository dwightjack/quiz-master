import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import FaPlus from 'react-icons/lib/fa/plus';
import bindAll from 'lodash/bindAll';

import { QuestionType } from '../../shared/types';
import AdminQuestion from '../../components/AdminQuestion';
import Button from '../../objects/Button';
import Well from '../../objects/Well';

import {
    saveQuestionAction,
    fetchQuestionsAction,
    createEmptyQuestionAction,
    deleteQuestionAction,
    clearQuestionAction
} from './actions';

import './_admin.scss';

export default class Admin extends Component {

    constructor(props) {
        super(props);

        bindAll(this, ['addQuestion']);
    }

    componentDidMount() {
        this.props.fetchAction();
    }

    addQuestion() {
        this.props.createAction();
    }

    renderQuestions() {
        const { questions } = this.props;

        if (questions.length === 0) {
            return (
                <Well>
                    <p>Looks like you didn&rsquo;t add any question yet.</p>
                    <p> Click on &quot;Add question&quot; below to get started!</p>
                </Well>
            );
        }

        return questions.map((question, idx) => (
            <AdminQuestion
                onSave={this.props.saveAction}
                onDelete={this.props.deleteAction}
                question={question}
                key={question._uid}
                idx={idx}
            />
        ));
    }

    render() {

        return (
            <div className="t-admin c-admin">
                <h1 className="h1" id="main-title">Quiz Master Admin</h1>
                <main role="main" id="main">
                    <section className="c-admin__question-list">

                        {this.renderQuestions()}

                        <footer>
                            <Button className="o-button--action" onClick={this.addQuestion}><FaPlus /> Add Question</Button>
                        </footer>

                    </section>
                </main>
            </div>
        );
    }

}

Admin.propTypes = {
    saveAction: PropTypes.func,
    fetchAction: PropTypes.func,
    createAction: PropTypes.func,
    deleteAction: PropTypes.func,
    questions: PropTypes.arrayOf(QuestionType)
};

Admin.defaultProps = {
    questions: []
};

const mapStateToProps = ({ admin }) => ({ questions: admin.questions });

const mapDispatchToProps = (dispatch) => ({
    createAction() {
        dispatch(createEmptyQuestionAction());
    },
    deleteAction(question) {
        if (question.id) {
            //has a database ID
            dispatch(deleteQuestionAction(question.id, question._uid));
        } else {
            dispatch(clearQuestionAction(question._uid));
        }
    },
    saveAction(question) {
        dispatch(saveQuestionAction(question));
    },
    fetchAction() {
        dispatch(fetchQuestionsAction());
    }
});


export const ConnectedAdmin = connect(
    mapStateToProps,
    mapDispatchToProps
)(Admin);