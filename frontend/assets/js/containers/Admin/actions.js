import shortid from 'shortid';

import api from '../../shared/api';
import { API_ENDPOINT_QUESTIONS } from '../../shared/constants';

export const QUESTION_CREATE = 'admin/questions/CREATE';

export const QUESTION_GET_START = 'admin/questions/GET_START';
export const QUESTION_GET_COMPLETE = 'admin/questions/GET_COMPLETE';
export const QUESTION_GET_ERROR = 'admin/questions/GET_ERROR';

export const QUESTION_POST_START = 'admin/questions/POST_START';
export const QUESTION_POST_COMPLETE = 'admin/questions/POST_COMPLETE';
export const QUESTION_POST_ERROR = 'admin/questions/POST_ERROR';

export const QUESTION_DELETE_START = 'admin/questions/DELETE_START';
export const QUESTION_DELETE_COMPLETE = 'admin/questions/DELETE_COMPLETE';
export const QUESTION_DELETE_ERROR = 'admin/questions/DELETE_ERROR';

export const QUESTION_CLEAR = 'admin/questions/CLEAR';

export const createEmptyQuestionAction = () => ({
    type: QUESTION_CREATE,
    payload: {
        type: 'text',
        body: '',
        answer: '',
        options: [],
        _uid: shortid.generate()
    }
});

export const fetchQuestionsAction = () => { //eslint-disable-line arrow-body-style

    return {
        types: [
            QUESTION_GET_START,
            QUESTION_GET_COMPLETE,
            QUESTION_GET_ERROR
        ],
        shouldCallAPI: ({ admin }) => admin.questions.length === 0,
        callAPI: () => api.get(API_ENDPOINT_QUESTIONS, { params: { admin: '1' } })
    };
};

export const saveQuestionAction = (payload = {}) => {

    const { id } = payload;
    const method = id ? 'put' : 'post';
    const callAPI = api[method].bind(api, API_ENDPOINT_QUESTIONS + (id || ''), payload);

    return {
        types: [
            QUESTION_POST_START,
            QUESTION_POST_COMPLETE,
            QUESTION_POST_ERROR
        ],
        payload,
        callAPI
    };
};

export const deleteQuestionAction = (id, _uid) => ({
    types: [
        QUESTION_DELETE_START,
        QUESTION_DELETE_COMPLETE,
        QUESTION_DELETE_ERROR
    ],
    payload: { id, _uid },
    callAPI: () => api.delete(API_ENDPOINT_QUESTIONS + id)
});

export const clearQuestionAction = (_uid) => ({
    type: QUESTION_CLEAR,
    payload: { _uid }
});