import * as actions from './actions';
import * as reducers from './reducers';
import { questions, emptyQuestion } from '../../__fixtures__/questions';


/* eslint-env jest */
describe('<Admin> reducers', () => {

    describe('questionsReducer', () => {

        it('should return an empty Array', () => {
            const nextState = reducers.questionsReducer(undefined, { type: null });
            expect(nextState).toEqual(expect.any(Array));
            expect(nextState.length).toBe(0);
        });

    });


    describe('questionsReducer `QUESTION_POST_COMPLETE`', () => {

        let mockAction;

        beforeEach(() => {
            mockAction = {
                type: actions.QUESTION_POST_COMPLETE,
                payload: {}
            };
        });


        it('should pass when payload is falsy', () => {
            const initialState = [];
            const nextState = reducers.questionsReducer(
                initialState,
                mockAction
            );
            expect(nextState).toBe(initialState);

            const nextState2 = reducers.questionsReducer(
                initialState,
                Object.assign(mockAction, { payload: null })
            );
            expect(nextState2).toBe(initialState);

        });


        it('should pass if returned question payload has no match in the store', () => {

            const initialState = [...questions];
            const payload = Object.assign({ id: 200000 }, emptyQuestion);
            const nextState = reducers.questionsReducer(
                initialState,
                Object.assign(mockAction, { payload })
            );
            expect(nextState).toBe(initialState);

        });


        it('should update state if returned question payload matches by `_uid` the store', () => {

            const initialState = [...questions];
            const payload = Object.assign({ _uid: questions[0]._uid }, emptyQuestion);
            const nextState = reducers.questionsReducer(
                initialState,
                Object.assign(mockAction, { payload })
            );
            expect(nextState).toContainEqual(payload);

        });


    });


    describe('questionsReducer `QUESTION_CREATE`', () => {

        it('should pass on falsy payload', () => {
            const initialState = [...questions];
            const nextState = reducers.questionsReducer(initialState, {
                type: actions.QUESTION_CREATE,
                payload: null
            });

            expect(nextState).toBe(initialState);
        });


        it('should enqueue a question', () => {
            const initialState = [...questions];
            const nextState = reducers.questionsReducer(initialState, {
                type: actions.QUESTION_CREATE,
                payload: emptyQuestion
            });

            const expected = nextState.length - 1;
            const idx = nextState.findIndex((q) => q === emptyQuestion);
            expect(idx).toBe(expected);
        });
    });



    describe('questionsReducer `QUESTION_GET_COMPLETE`', () => {

        const type = actions.QUESTION_GET_COMPLETE;

        it('should allow just array payloads', () => {
            const initialState = [...questions];
            const nextState = reducers.questionsReducer(initialState, { type, payload: 'a string' });
            expect(nextState).toBe(initialState);
        });


        it('should replace the payload on QUESTION_GET_COMPLETE', () => {
            const initialState = [...questions];
            const payload = [emptyQuestion];
            const nextState = reducers.questionsReducer(initialState, { type, payload });
            expect(nextState).not.toContainEqual(questions[0]);
        });


        it('should add a _uid property to payload on QUESTION_GET_COMPLETE', () => {
            const initialState = [];
            const payload = [Object.assign({}, emptyQuestion)];
            const nextState = reducers.questionsReducer(initialState, { type, payload });
            expect(nextState[0]._uid).toEqual(expect.any(String));

        });

    });



    describe('questionsReducer `QUESTION_DELETE_COMPLETE`', () => {

        const type = actions.QUESTION_DELETE_COMPLETE;

        it('should allow just payload with `._uid` property', () => {
            const initialState = [...questions];
            const payloadEmpty = null;
            const payload = { _uid: 'string' };
            const nextState = reducers.questionsReducer(initialState, { type, payload: payloadEmpty });
            expect(nextState).toEqual(initialState);

            const nextState2 = reducers.questionsReducer(initialState, { type, payload });
            expect(nextState2).toEqual(initialState);
        });


        it('should return the state without the removed question', () => {
            const initialState = [...questions];
            const payload = { _uid: questions[0]._uid };
            const nextState = reducers.questionsReducer(initialState, { type, payload });
            expect(nextState).not.toContainEqual(questions[0]);
        });


    });



    describe('questionsReducer `QUESTION_CLEAR`', () => {

        const type = actions.QUESTION_CLEAR;

        it('should allow just payload with `._uid` property', () => {
            const initialState = [...questions];
            const payloadEmpty = null;
            const payload = { _uid: 'string' };
            const nextState = reducers.questionsReducer(initialState, { type, payload: payloadEmpty });
            expect(nextState).toEqual(initialState);

            const nextState2 = reducers.questionsReducer(initialState, { type, payload });
            expect(nextState2).toEqual(initialState);
        });


        it('should return the state without the removed question', () => {
            const initialState = [...questions];
            const payload = { _uid: questions[0]._uid };
            const nextState = reducers.questionsReducer(initialState, { type, payload });
            expect(nextState).not.toContainEqual(questions[0]);
        });


    });



});