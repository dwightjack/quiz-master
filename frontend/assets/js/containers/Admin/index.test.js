import React from 'react';
import { shallow } from 'enzyme';

import Admin from './index';

/* eslint-env jest, jasmine */
describe('<Admin>', () => {

    const questions = [{
        _uid: '1',
        body: '<p>question-body</p>',
        type: 'text',
        id: 10,
        options: []
    }, {
        _uid: '2',
        body: '<p>question-body</p>',
        type: 'pick',
        id: 10,
        options: ['1', '2', '3']
    }];

    describe('.addQuestion()', () => {

        it('should call createAction prop', () => {
            const spy = jest.fn();
            const wrapper = shallow(<Admin createAction={spy} />);

            wrapper.instance().addQuestion();
            expect(spy).toHaveBeenCalled();
        });

    });

    describe('.renderQuestions()', () => {

        let wrapper;
        let instance;
        let saveAction;
        let deleteAction;


        beforeEach(() => {
            saveAction = jest.fn();
            deleteAction = jest.fn();
            wrapper = shallow(
                <Admin questions={questions} deleteAction={deleteAction} saveAction={saveAction} />
            );


            instance = wrapper.instance();
        });


        it('should render a list of <AdminQuestion> instances', () => {

            const list = instance.renderQuestions();

            const listWrapp = shallow(<div>{list}</div>);

            expect(listWrapp.find('AdminQuestion').length).toBe(questions.length);


            list.forEach((l, i) => {
                expect(l.props.onSave).toBe(saveAction);
                expect(l.props.onDelete).toBe(deleteAction);
                expect(l.props.question).toBe(questions[i]);
                expect(l.props.idx).toBe(i);
            });

        });


        it('should render a <Well> on empty questions', () => {
            const emptyWrapper = shallow(<Admin />);
            const res = emptyWrapper.instance().renderQuestions();

            expect(shallow(res).is('.o-well')).toBe(true);

        });

    });

    describe('.componentDidMount()', () => {

        it('should call .fetchAction() prop', () => {
            const spy = jest.fn();
            const wrapper = shallow(<Admin fetchAction={spy} />);

            wrapper.instance().componentDidMount();

            expect(spy).toHaveBeenCalled();

        });
    });


    describe('.render()', () => {

        let wrapper;
        let instance;

        beforeEach(() => {
            wrapper = shallow(
                <Admin questions={questions} />
            );
            instance = wrapper.instance();
        });


        it('should render a .c-admin.t-admin root element', () => {
            expect(wrapper.is('.c-admin.t-admin')).toBe(true);
        });


        it('should render a AdminQuestion list', () => {

            spyOn(instance, 'renderQuestions');

            instance.render();
            expect(instance.renderQuestions).toHaveBeenCalled();
            expect(wrapper.find('AdminQuestion').length).toBe(questions.length);

        });


        it('should render a Button to add a question', () => {

            expect(
                wrapper.find('Button').prop('onClick')
            ).toBe(instance.addQuestion);

        });

    });

});