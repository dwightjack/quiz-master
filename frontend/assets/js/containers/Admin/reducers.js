import { combineReducers } from 'redux';
import isEmpty from 'lodash/isEmpty';
import shortid from 'shortid';

import {
    QUESTION_POST_COMPLETE,
    QUESTION_CREATE,
    QUESTION_GET_COMPLETE,
    QUESTION_DELETE_COMPLETE,
    QUESTION_CLEAR
} from './actions';

const initialState = [];

export const questionsReducer = (state = initialState, { type, payload }) => { //eslint-disable-line

    switch (type) {

    case QUESTION_POST_COMPLETE:
        if (isEmpty(payload) === false) {
            //replace existing question...
            const questionIdx = state.findIndex((q) => q._uid === payload._uid);
            if (questionIdx !== -1) {
                return [
                    ...state.slice(0, questionIdx),
                    payload,
                    ...state.slice(questionIdx + 1)
                ];
            }
        }

        break;

    case QUESTION_CREATE:
        return payload ? [...state, payload] : state;

    case QUESTION_GET_COMPLETE:
        if (Array.isArray(payload)) {
            return payload.map((question) => Object.assign({
                _uid: shortid.generate()
            }, question));
        }
        break;

    case QUESTION_DELETE_COMPLETE:
    case QUESTION_CLEAR:
        if (payload && payload._uid) {
            //replace existing question...
            return state.filter((q) => q._uid !== payload._uid);
        }
        break;

    default:
        return state;
    }

    return state;
};

export default combineReducers({
    questions: questionsReducer
});