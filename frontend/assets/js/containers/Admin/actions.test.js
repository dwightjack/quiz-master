import { API_ENDPOINT_QUESTIONS } from '../../shared/constants';
import api from '../../shared/api';
import * as action from './actions';
import { emptyQuestion } from '../../__fixtures__/questions';

jest.mock('../../shared/api');


/* eslint-env jest */
describe('<Admin> actions', () => {

    describe('createEmptyQuestionAction', () => {

        it('should return a type `QUESTION_CREATE`', () => {
            expect(action.createEmptyQuestionAction().type).toBe(action.QUESTION_CREATE);
        });

        it('should have an empty question as payload', () => {

            expect(action.createEmptyQuestionAction().payload).toMatchObject(emptyQuestion);
            //should have an auto-generated string _uid
            expect(action.createEmptyQuestionAction().payload._uid).toEqual(expect.any(String));
            //should not have an id
            expect(action.createEmptyQuestionAction().payload.id).toBeUndefined();

        });

    });


    describe('fetchQuestionsAction', () => {

        it('should return a types Array', () => {
            expect(action.fetchQuestionsAction().types).toEqual(expect.any(Array));
        });


        it('should have start, complete and error types ', () => {
            expect(action.fetchQuestionsAction().types).toEqual([
                action.QUESTION_GET_START,
                action.QUESTION_GET_COMPLETE,
                action.QUESTION_GET_ERROR
            ]);
        });


        it('should have a shouldCallAPI that returns `true` if question list is empty', () => {
            const state = { admin: { questions: [] } };
            expect(
                action.fetchQuestionsAction().shouldCallAPI(state)
            ).toBe(true);
        });


        it('should have a shouldCallAPI that returns `false` if question list is populated', () => {
            const state = { admin: { questions: [1, 2] } };
            expect(
                action.fetchQuestionsAction().shouldCallAPI(state)
            ).toBe(false);
        });


        it('should have a callAPI function which triggers a api.get', () => {
            expect(action.fetchQuestionsAction().callAPI).toEqual(expect.any(Function));

            action.fetchQuestionsAction().callAPI();

            expect(api.get).toHaveBeenCalledWith(
                API_ENDPOINT_QUESTIONS,
                { params: { admin: '1' } }
            );
        });

    });


    describe('saveQuestionAction', () => {

        it('should return a types Array', () => {
            expect(action.saveQuestionAction().types).toEqual(expect.any(Array));
        });

        it('should have start, complete and error types ', () => {
            expect(action.saveQuestionAction().types).toEqual([
                action.QUESTION_POST_START,
                action.QUESTION_POST_COMPLETE,
                action.QUESTION_POST_ERROR
            ]);
        });


        it('should have a passed-in question object as payload', () => {
            expect(action.saveQuestionAction(emptyQuestion).payload).toEqual(emptyQuestion);
        });


        it('should have a POST callAPI method when passed-in question has no `id` field', () => {
            action.saveQuestionAction(emptyQuestion).callAPI();
            expect(api.post).toHaveBeenCalledWith(
                API_ENDPOINT_QUESTIONS,
                emptyQuestion
            );
        });


        it('should have a PUT callAPI method when passed-in question has no `id` field', () => {
            const updateQuestion = Object.assign({
                id: 10
            }, emptyQuestion);

            action.saveQuestionAction(updateQuestion).callAPI();
            expect(api.put).toHaveBeenCalledWith(
                API_ENDPOINT_QUESTIONS + updateQuestion.id,
                updateQuestion
            );

        });


    });


    describe('deleteQuestionAction', () => {

        it('should return a types Array', () => {
            expect(action.deleteQuestionAction().types).toEqual(expect.any(Array));
        });

        it('should have start, complete and error types ', () => {
            expect(action.deleteQuestionAction().types).toEqual([
                action.QUESTION_DELETE_START,
                action.QUESTION_DELETE_COMPLETE,
                action.QUESTION_DELETE_ERROR
            ]);
        });


        it('should pass `id` and `_uid` as pyaload', () => {
            const id = 10;
            const uid = 'uid-string';
            const expected = { id, _uid: uid };

            expect(action.deleteQuestionAction(id, uid).payload).toEqual(expected);
        });


        it('should have a DELETE callAPI function pointing to the question ID api endpoint', () => {
            action.deleteQuestionAction(10, '_string').callAPI();
            expect(api.delete).toHaveBeenCalledWith(
                `${API_ENDPOINT_QUESTIONS}10`
            );
        });

    });

    describe('.clearQuestionAction()', () => {

        it('should return a type `QUESTION_CLEAR`', () => {
            expect(action.clearQuestionAction().type).toBe(action.QUESTION_CLEAR);
        });


        it('should have the question `_uid` as payload', () => {
            const _uid = '_uid-string';
            expect(action.clearQuestionAction(_uid).payload).toEqual({ _uid });
        });

    });

});