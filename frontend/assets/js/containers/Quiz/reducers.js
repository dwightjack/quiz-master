import shortid from 'shortid';

import {
    QUESTION_GET_COMPLETE,
    ANSWER_RESET,
    ANSWER_SAVE
} from './actions';

const initialState = [];

export const questionsReducer = (state = initialState, { type, payload }) => {

    switch (type) {

    case QUESTION_GET_COMPLETE:
        if (Array.isArray(payload)) {
            return payload.map((question) => Object.assign({
                _uid: shortid.generate()
            }, question));
        }
        return state;

    default:
        return state;
    }
};


export const answersReducer = (state = initialState, { type, payload }) => {

    switch (type) {

    case ANSWER_RESET:
        return initialState;

    case ANSWER_SAVE: //eslint-disable-line
        //get previous answer...
        const prevAnswerIdx = state.findIndex((answer) => answer.qid === payload.qid);
        if (prevAnswerIdx !== -1) {
            return [
                ...state.slice(0, prevAnswerIdx),
                payload,
                ...state.slice(prevAnswerIdx + 1)
            ];
        }
        return [...state, payload];

    default:
        return state;
    }
};