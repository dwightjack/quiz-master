import { API_ENDPOINT_QUESTIONS } from '../../shared/constants';
import api from '../../shared/api';
import * as action from './actions';

jest.mock('../../shared/api');


/* eslint-env jest */
describe('<Quiz> actions', () => {

    describe('resetAnswerAction', () => {

        it('should return a type `ANSWER_RESET`', () => {
            expect(action.resetAnswerAction().type).toBe(action.ANSWER_RESET);
        });

    });


    describe('saveAnswerAction', () => {

        it('should return a type `ANSWER_SAVE` with payload', () => {
            expect(action.saveAnswerAction(10, 'test').type).toBe(action.ANSWER_SAVE);
            expect(action.saveAnswerAction(10, 'test').payload).toEqual({ qid: 10, answer: 'test' });
        });

    });


    describe('fetchQuestionsAction', () => {

        it('should return a types Array', () => {
            expect(action.fetchQuestionsAction().types).toEqual(expect.any(Array));
        });

        it('should have start, complete and error types ', () => {
            expect(action.fetchQuestionsAction().types).toEqual([
                action.QUESTION_GET_START,
                action.QUESTION_GET_COMPLETE,
                action.QUESTION_GET_ERROR
            ]);
        });

        it('should have a callAPI function which triggers a api.get', () => {
            expect(action.fetchQuestionsAction().callAPI).toEqual(expect.any(Function));

            action.fetchQuestionsAction().callAPI();

            expect(api.get).toHaveBeenCalledWith(API_ENDPOINT_QUESTIONS);
        });

    });

});