import * as actions from './actions';
import * as reducers from './reducers';
import { questions, emptyQuestion } from '../../__fixtures__/questions';
import { answers } from '../../__fixtures__/answers';


/* eslint-env jest */
describe('<Quiz> reducers', () => {

    describe('questionsReducer', () => {

        it('should return an empty Array', () => {
            const nextState = reducers.questionsReducer(undefined, { type: null });
            expect(nextState).toEqual(expect.any(Array));
            expect(nextState.length).toBe(0);
        });

        it('should allow just array payloads', () => {
            const initial = [{ body: 'test' }];
            const nextState = reducers.questionsReducer(initial, { payload: 'a string' });
            expect(nextState).toBe(initial);
        });

        it('should replace the payload on QUESTION_GET_COMPLETE', () => {
            const initial = questions;
            const payload = [emptyQuestion];
            const nextState = reducers.questionsReducer(initial, { type: actions.QUESTION_GET_COMPLETE, payload });
            expect(nextState).not.toContain(questions[0]);
        });

        it('should add a _uid property to payload on QUESTION_GET_COMPLETE', () => {
            const initial = [];
            const payload = [Object.assign({}, emptyQuestion)];
            const nextState = reducers.questionsReducer(initial, { type: actions.QUESTION_GET_COMPLETE, payload });
            expect(nextState[0]._uid).toEqual(expect.any(String));
        });

    });



    describe('answersReducer', () => {

        let initial;

        beforeEach(() => {
            initial = [...answers];
        });

        it('should return an empty Array', () => {
            const nextState = reducers.answersReducer(undefined, { type: null });

            expect(nextState).toEqual(expect.any(Array));
            expect(nextState.length).toBe(0);
        });


        it('should empty the answers on ANSWER_RESET', () => {
            const nextState = reducers.answersReducer(initial, { type: actions.ANSWER_RESET });
            expect(nextState.length).toBe(0);
        });


        it('should enqueue the new answer if not present (by qid)', () => {
            const payload = Object.assign({}, answers[1], { qid: 100000 });
            const nextState = reducers.answersReducer(initial, { type: actions.ANSWER_SAVE, payload });
            const expected = initial.length + 1;

            expect(nextState.length).toBe(expected);

        });


        it('should replace the answer if already present (by qid)', () => {
            const payload = Object.assign({}, answers[0], { answer: '---string' });
            const nextState = reducers.answersReducer(initial, { type: actions.ANSWER_SAVE, payload });

            expect(nextState.length).toBe(initial.length);
            expect(nextState[0].answer).toBe(payload.answer);

        });

    });


});