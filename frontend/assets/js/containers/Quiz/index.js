import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import Link from 'react-router/lib/Link';
import bindAll from 'lodash/bindAll';
import noop from 'lodash/noop';
import FaChevronRight from 'react-icons/lib/fa/chevron-right';
import FaChevronLeft from 'react-icons/lib/fa/chevron-left';
import FaSmileO from 'react-icons/lib/fa/smile-o';
import shortid from 'shortid';

import Button from '../../objects/Button';
import Progress from '../../objects/Progress';
import Well from '../../objects/Well';

import { QuestionType, AnswerType } from '../../shared/types';
import Question from '../../components/Question';
import { fetchQuestionsAction, saveAnswerAction, resetAnswerAction } from './actions';

import './_quiz.scss';

export default class Quiz extends Component {


    constructor(props) {
        super(props);

        this.state = {
            currentQuestionIdx: 0,
            quizId: shortid.generate()
        };

        bindAll(this, ['prev', 'next', 'onAnswerChange', 'onKeyUp']);

        if (props.answers && props.answers.length > 0) {
            this.props.resetAction();
        }

    }

    componentDidMount() {
        this.props.fetchAction();
    }

    onAnswerChange(answer) {
        this.props.saveAnswerAction(answer);
    }

    onKeyUp(e) {
        if (e.key === 'Enter') {
            this.next();
        }
    }

    prev() {
        const { currentQuestionIdx } = this.state;

        if (currentQuestionIdx > 0) {
            this.setState({ currentQuestionIdx: (currentQuestionIdx - 1) });
        }
    }

    next() {
        const { currentQuestionIdx } = this.state;
        const { questions = [] } = this.props;

        if (currentQuestionIdx < questions.length) {
            this.setState({ currentQuestionIdx: (currentQuestionIdx + 1) });
        }

    }

    renderQuestion() {
        const { questions = [], answers = [] } = this.props;
        const { currentQuestionIdx } = this.state;

        if (questions.length === 0) {
            return null;
        }

        const currentQuestion = questions[currentQuestionIdx];
        const answer = answers.find(({ qid }) => currentQuestion && currentQuestion.id === qid);

        if (!currentQuestion) {
            return null;
        }
        /* eslint-disable jsx-a11y/no-static-element-interactions */
        return (
            <section className="c-quiz__question-list" onKeyUp={this.onKeyUp}>
                <Question
                    question={currentQuestion}
                    answer={answer}
                    onChange={this.onAnswerChange}
                    key={currentQuestion.id}
                    idx={currentQuestionIdx}
                />
            </section>
        );
        /* eslint-enable jsx-a11y/no-static-element-interactions */
    }

    render() {

        const { questions = [] } = this.props;
        const { currentQuestionIdx } = this.state;

        const hasQuestions = questions.length > 0;
        const question = hasQuestions ? this.renderQuestion() : null;

        const quizEnd = hasQuestions && (currentQuestionIdx === questions.length);

        return (
            <div className="c-quiz">
                <h1 className="h1 c-quiz__title" id="main-title">Quiz Master</h1>
                <main id="main" role="main" className="c-quiz__body">

                    {question}

                    {quizEnd && (
                        <Well className="c-quiz__message">
                            <h3 className="h5"><FaSmileO className="u-type--l" /> You made it through the Quiz!</h3>
                            <p>Review your answers or check results.</p>
                        </Well>
                    )}

                    {!hasQuestions && <Well className="o-well--error">
                        <h3 className="h5"> Looks like there is no any question to be answered!</h3>
                        <p>Head <Link to="/admin">here</Link> to set some</p>
                    </Well>}

                    <footer className={classnames('c-quiz__actions', { 'u-hidden': !hasQuestions })}>
                        <Button disabled={currentQuestionIdx === 0} onClick={this.prev}><FaChevronLeft /> Prev</Button>
                        {quizEnd === false && <Button onClick={this.next}>Next <FaChevronRight className="is-after" /></Button>}
                        {quizEnd && <Link className="o-button o-button--primary" to="/results">See results</Link>}
                    </footer>
                </main>
                {hasQuestions && <Progress className="c-quiz__progress" current={currentQuestionIdx} total={questions.length} />}
            </div>
        );
    }

}

Quiz.propTypes = {
    resetAction: PropTypes.func,
    saveAnswerAction: PropTypes.func,
    fetchAction: PropTypes.func,
    questions: PropTypes.arrayOf(QuestionType),
    answers: PropTypes.arrayOf(AnswerType)
};

Quiz.defaultProps = {
    resetAction: noop,
    saveAnswerAction: noop,
    fetchAction: noop
};

const mapStateToProps = ({ questions, answers }) => ({ questions, answers });

const mapDispatchToProps = (dispatch) => ({

    saveAnswerAction({ qid, answer }) {
        dispatch(saveAnswerAction(qid, answer));
    },

    resetAction() {
        dispatch(resetAnswerAction());
    },

    fetchAction() {
        dispatch(fetchQuestionsAction());
    }
});

export const ConnectedQuiz = connect(
    mapStateToProps,
    mapDispatchToProps
)(Quiz);