import api from '../../shared/api';
import { API_ENDPOINT_QUESTIONS } from '../../shared/constants';

export const QUESTION_GET_START = 'quiz/questions/GET_START';
export const QUESTION_GET_COMPLETE = 'quiz/questions/GET_COMPLETE';
export const QUESTION_GET_ERROR = 'quiz/questions/GET_ERROR';

export const ANSWER_RESET = 'quiz/answers/RESET';
export const ANSWER_SAVE = 'quiz/answers/SAVE';

export const fetchQuestionsAction = () => ({
    types: [
        QUESTION_GET_START,
        QUESTION_GET_COMPLETE,
        QUESTION_GET_ERROR
    ],
    callAPI: () => api.get(API_ENDPOINT_QUESTIONS)
});

export const resetAnswerAction = () => ({
    type: ANSWER_RESET
});

export const saveAnswerAction = (qid, answer) => ({
    type: ANSWER_SAVE,
    payload: { qid, answer }
});