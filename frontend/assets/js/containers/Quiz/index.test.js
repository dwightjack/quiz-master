import React from 'react';
import { shallow, mount } from 'enzyme';

import Well from '../../objects/Well';
import Question from '../../components/Question';
import Quiz from './index';

/* eslint-env jest, jasmine */
describe('<Quiz>', () => {

    const questions = [{
        id: 1,
        type: 'text',
        body: 'the question?',
        options: []
    }, {
        id: 2,
        type: 'pick',
        body: 'the question?',
        options: [1, 2, 3]
    }];

    const answers = [{ qid: 1, answer: 'the answer' }];

    describe('constructor', () => {

        it('should populate state with an idx and a unique id', () => {
            const quiz = shallow(<Quiz />);
            const state = quiz.state();
            expect(state.currentQuestionIdx).toBe(0);
            expect(state.quizId).toEqual(expect.any(String));
        });


        it('should call `resetAction` prop when `answers` prop is provided and populated', () => {
            const spy = jest.fn();
            shallow(<Quiz resetAction={spy} answers={[1, 2]} />);
            expect(spy).toHaveBeenCalled();
        });

        it('should NOT call `resetAction` prop when `answers` prop is provided and populated', () => {
            const spy = jest.fn();
            shallow(<Quiz resetAction={spy} />);
            expect(spy).not.toHaveBeenCalled();
        });

    });

    describe('.next()', () => {

        it('should update `currentQuestionIdx` state', () => {
            const quiz = shallow(<Quiz questions={questions} />);
            const instance = quiz.instance();

            instance.next();
            expect(quiz.state('currentQuestionIdx')).toBe(1);

        });

        it('should NOT update state when question length is exceeded', () => {
            const quiz = shallow(<Quiz questions={questions} />);
            const instance = quiz.instance();

            quiz.setState({ currentQuestionIdx: questions.length });

            spyOn(instance, 'setState');
            instance.next();
            expect(instance.setState).not.toHaveBeenCalled();

        });

    });


    describe('.prev()', () => {

        it('should update `currentQuestionIdx` state', () => {
            const quiz = shallow(<Quiz questions={questions} />);
            const instance = quiz.instance();
            quiz.setState({ currentQuestionIdx: 1 });
            instance.prev();
            expect(quiz.state('currentQuestionIdx')).toBe(0);

        });

        it('should NOT update state when `currentQuestionIdx` === 0', () => {
            const quiz = shallow(<Quiz questions={questions} />);
            const instance = quiz.instance();

            spyOn(instance, 'setState');
            instance.prev();
            expect(instance.setState).not.toHaveBeenCalled();

        });

    });


    describe('.onAnswerChange()', () => {

        it('should call `saveAnswerAction` prop', () => {
            const spy = jest.fn();
            const quiz = shallow(<Quiz saveAnswerAction={spy} />);
            const instance = quiz.instance();
            const mock = answers[0];

            instance.onAnswerChange(mock);
            expect(spy).toHaveBeenCalledWith(mock);

        });

    });

    describe('.onKeyUp()', () => {

        it('should call `.next() when a Enter keyboard event is passed-in`', () => {
            const quiz = shallow(<Quiz />);
            const instance = quiz.instance();
            spyOn(instance, 'next');

            instance.onKeyUp({ key: 'Enter' });
            expect(instance.next).toHaveBeenCalledWith();

        });


        it('should NOT call `.next() when a Enter keyboard event is passed-in`', () => {
            const quiz = shallow(<Quiz />);
            const instance = quiz.instance();
            spyOn(instance, 'next');

            instance.onKeyUp({ key: 'Left' });
            expect(instance.next).not.toHaveBeenCalledWith();

        });

    });


    describe('.render()', () => {

        it('should render a `div.c-quiz` root element', () => {
            const quiz = shallow(<Quiz />);
            expect(quiz.is('div.c-quiz')).toBe(true);
        });


        it('should provide an error well when no questions are provided', () => {
            const quiz = shallow(<Quiz />);
            expect(quiz.find(Well).is('.o-well--error')).toBe(true);
        });


        it('should hide quiz actions on no questions', () => {
            const quiz = shallow(<Quiz />);
            expect(quiz.find('.c-quiz__actions.u-hidden').length).toBe(1);
        });


        it('should render the first passed question on first render', () => {
            const quiz = shallow(<Quiz questions={questions} />);
            expect(quiz.find(Question).length).toBe(1);
            expect(quiz.find(Question).prop('question')).toEqual(questions[0]);
        });


        it('should pass a previous answer if matched', () => {
            const quiz = shallow(<Quiz questions={questions} answers={answers} />);
            const question = quiz.find(Question);
            expect(question.length).toBe(1);
            expect(question.prop('question')).toEqual(questions[0]);
            expect(question.prop('answer')).toEqual(answers[0]);
        });


        it('should NOT render any question component on first render if `questions` is not provided', () => {
            const quiz = shallow(<Quiz />);
            expect(quiz.find(Question).length).toBe(0);
        });

    });

    describe('Lifecycle and interactions', () => {

        let quiz;
        let fetchAction;
        let saveAnswerAction;
        let instance;

        beforeEach(() => {
            fetchAction = jest.fn();
            saveAnswerAction = jest.fn();
            quiz = mount(<Quiz questions={questions} answers={answers} fetchAction={fetchAction} saveAnswerAction={saveAnswerAction} />);
            instance = quiz.instance();
        });


        it('should fetch questions on mount', () => {
            expect(fetchAction).toHaveBeenCalled();
        });


        it('should disable prev button on first render', () => {
            expect(quiz.find('Button').at(0).prop('disabled')).toBe(true);
        });


        it('should render a Progress', () => {
            expect(quiz.find('Progress').length).toBe(1);
            expect(quiz.find('Progress').prop('total')).toBe(quiz.prop('questions').length);
        });


        it('should update Progress `current` on state update', () => {
            quiz.setState({ currentQuestionIdx: 1 });
            expect(quiz.find('Progress').prop('current')).toBe(1);
        });


        it('should call `next` on next button click', () => {
            const onClick = quiz.find('Button').at(1).prop('onClick');
            expect(onClick).toBe(instance.next);

        });


        it('should call `prev` on prev button click', () => {
            const onClick = quiz.find('Button').at(0).prop('onClick');
            expect(onClick).toBe(instance.prev);
        });


        it('should provide an end message at last page', () => {
            quiz.setState({ currentQuestionIdx: questions.length });
            expect(quiz.find('.c-quiz__message').length).toBe(1);
        });


        it('should hide next button on last page', () => {
            quiz.setState({ currentQuestionIdx: questions.length });
            expect(quiz.find('.c-quiz__actions').find('Button').length).toBe(1);
        });


        it('should provide a go-to-result link on last page', () => {
            quiz.setState({ currentQuestionIdx: questions.length });
            expect(quiz.find('Link').last().prop('to')).toBe('/results');
        });
    });

});