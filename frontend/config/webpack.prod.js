const webpack = require('webpack');
const _ = require('lodash');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const webpackConf = require('./webpack.base');
const paths = require('./paths');

const getAssetPath = require('../scripts/utils').getAssetPath;

const config = _.assign(webpackConf, {

    entry: {
        app: [
            './' + paths.toPath('src.assets/js') + '/app.styles.js',
            './' + paths.toPath('src.assets/js') + '/app.js'
        ],
        vendor: [
            'babel-polyfill',
            'shortid',
            'react',
            'axios',
            'classnames',
            'react-dom',
            'hoist-non-react-statics',
            'react-redux',
            'react-rte',
            'redux',
            'redux-thunk'
        ]
    },

    output: _.assign({}, webpackConf.output, {
        filename: paths.js + '/[name].[chunkhash:10].js',
        chunkFilename: paths.js + '/[name].[chunkhash:10].chunk.js'
    }),

    devtool: '#source-map'
});

config.plugins.push(
    new webpack.optimize.CommonsChunkPlugin({
        name: 'vendor',
        minChunks: Infinity
    }),
    new webpack.optimize.UglifyJsPlugin({
        sourceMap: true,
        compressor: {
            warnings: false
        }
    }),
    new webpack.LoaderOptionsPlugin({
        minimize: true
    }),

    new ExtractTextPlugin({
        filename: paths.css + '/[name].[contenthash:10].css'
    }),

    new HtmlWebpackPlugin({
        template: paths.toPath('src.root/templates') + '/index.ejs',
        filename: paths.toAbsPath('dist.root') + '/index.html',
        minify: {
            removeComments: true,
            collapseWhitespace: true,
            removeRedundantAttributes: true,
            useShortDoctype: true,
            removeEmptyAttributes: false,
            removeStyleLinkTypeAttributes: true,
            keepClosingSlash: true,
            minifyJS: true,
            minifyCSS: true,
            minifyURLs: true
        },
        modernizr: getAssetPath('vendors/modernizr/modernizr.*'),
        inject: true
    })
);


module.exports = config;