const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');
const _ = require('lodash');

const getAssetPath = require('../scripts/utils').getAssetPath;
const paths = require('./paths');
const webpackConf = require('./webpack.base');

const config = _.assign(webpackConf, {

    entry: {
        app: [
            './' + paths.toPath('src.assets/js') + '/app.styles.js',
            './' + paths.toPath('src.assets/js') + '/app.js'
        ]
    },

    devtool: '#source-map',

    plugins: webpackConf.plugins.concat([

        new webpack.LoaderOptionsPlugin({
            debug: true
        }),

        new HtmlWebpackPlugin({
            template: paths.toPath('src.root/templates') + '/index.ejs',
            inject: true,
            minify: false,
            alwaysWriteToDisk: true,
            filename: paths.toAbsPath('dist.root') + '/index.html',
            modernizr: getAssetPath('vendors/modernizr/modernizr.*')
        }),
        new HtmlWebpackHarddiskPlugin()
    ])

});

module.exports = config;