const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const paths = require('./paths');

const PRODUCTION = process.env.NODE_ENV === 'production';
const srcPath = paths.toAbsPath('src.assets');
const destPath = paths.toAbsPath('dist.assets');

const sassFunctions = require('../scripts/sass-functions');

const styleLoaders = [
    { loader: 'css-loader', options: { sourceMap: true } },
    { loader: 'resolve-url-loader', options: { sourceMap: true } },
    { loader: 'postcss-loader' },
    { loader: 'sass-loader' }
];

module.exports = {
    context: process.cwd(),
    externals: {},
    entry: {},
    target: 'web', // Make web variables accessible to webpack, e.g. window
    stats: true,
    output: {
        path: destPath,
        publicPath: paths.publicPath,
        chunkFilename: paths.js + '/[name].chunk.js',
        filename: paths.js + '/[name].js'
    },
    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.LoaderOptionsPlugin({
            test: /\.scss$/,
            options: {
                context: process.cwd(),
                output: { path: destPath },
                sassLoader: {
                    sourceMap: true,
                    precision: 10,
                    includePaths: [
                        paths.toAbsPath('src.assets/sass'),
                        'node_modules'
                    ],
                    functions: sassFunctions,
                    outputStyle: 'expanded'
                }
            }
        }),
        new webpack.DefinePlugin({
            __PRODUCTION__: PRODUCTION,
            'process.env': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV)
            }
        })
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                include: [srcPath],
                loader: 'babel-loader'
            }, {
                test: /\.html$/,
                exclude: /(node_modules)/,
                loader: 'raw-loader'
            }, {
                test: /\.json$/,
                exclude: /(node_modules)/,
                loader: 'json-loader'
            }, {
                test: /\.(scss|css)$/,
                exclude: /(node_modules)/,
                use: (PRODUCTION ? ExtractTextPlugin.extract({
                    fallbackLoader: 'style-loader',
                    loader: styleLoaders
                }) : ['style-loader'].concat(styleLoaders))
            }, {
                test: /\.(eot|svg|ttf|woff|woff2|jpg|png|gif|svg)$/,
                include: [
                    paths.toAbsPath('src.assets/images'),
                    paths.toAbsPath('src.assets/fonts')
                ],
                loader: 'file-loader',
                options: {
                    name: (PRODUCTION ? '[path][name].[hash:10].[ext]' : '[path][name].[ext]'),
                    context: paths.toPath('src.assets')
                }
            }
        ]
    },

    resolve: {
        alias: {
            scss: paths.toAbsPath('src.assets/sass'),
            images: paths.toAbsPath('src.assets/images'),
            base: paths.toAbsPath('src.assets/js/base')
        },
        modules: ['node_modules']
    }
};