Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :answers
    end
  end
  namespace :api do
    namespace :v1 do
      resources :questions
    end
  end
  namespace :api do
    namespace :v1 do
      resources :users
    end
  end
  namespace :api do
    namespace :v1 do
      resources :quizzes
    end
  end
end