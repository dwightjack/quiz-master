class Api::V1::QuestionsController < ApplicationController
  before_action :set_api_v1_question, only: [:show, :update, :destroy]

  # GET /api/v1/questions
  def index
    @api_v1_questions = Api::V1::Question.all

    render json: @api_v1_questions
  end

  # GET /api/v1/questions/1
  def show
    render json: @api_v1_question
  end

  # POST /api/v1/questions
  def create
    @api_v1_question = Api::V1::Question.new(api_v1_question_params)

    if @api_v1_question.save
      render json: @api_v1_question, status: :created, location: @api_v1_question
    else
      render json: @api_v1_question.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/v1/questions/1
  def update
    if @api_v1_question.update(api_v1_question_params)
      render json: @api_v1_question
    else
      render json: @api_v1_question.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/v1/questions/1
  def destroy
    @api_v1_question.destroy
    render json: @api_v1_questions
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_api_v1_question
      @api_v1_question = Api::V1::Question.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def api_v1_question_params
      params.require(:question).permit(:type, :body, :answer, :options, :quiz_id)
    end
end
