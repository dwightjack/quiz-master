class Api::V1::QuizzesController < ApplicationController
  before_action :set_api_v1_quiz, only: [:show, :update, :destroy]

  # GET /api/v1/quizzes
  def index
    @api_v1_quizzes = Api::V1::Quiz.all

    render json: @api_v1_quizzes
  end

  # GET /api/v1/quizzes/1
  def show
    render json: @api_v1_quiz
  end

  # POST /api/v1/quizzes
  def create
    @api_v1_quiz = Api::V1::Quiz.new(api_v1_quiz_params)

    if @api_v1_quiz.save
      render json: @api_v1_quiz, status: :created, location: @api_v1_quiz
    else
      render json: @api_v1_quiz.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/v1/quizzes/1
  def update
    if @api_v1_quiz.update(api_v1_quiz_params)
      render json: @api_v1_quiz
    else
      render json: @api_v1_quiz.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/v1/quizzes/1
  def destroy
    @api_v1_quiz.destroy
    render json: @api_v1_quiz
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_api_v1_quiz
      @api_v1_quiz = Api::V1::Quiz.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def api_v1_quiz_params
      params.require(:quiz).permit(:title)
    end
end
