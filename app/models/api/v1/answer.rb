class Api::V1::Answer < ApplicationRecord
  belongs_to :question
  belongs_to :user
end
