class Api::V1::AnswerSerializer < ActiveModel::Serializer
  attributes :id, :answer, :timestamp
  has_one :question
  has_one :user
end
