class Api::V1::QuestionSerializer < ActiveModel::Serializer
  attributes :id, :type, :body, :answer, :options
  has_one :quiz
end
