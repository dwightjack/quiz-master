class Api::V1::QuizSerializer < ActiveModel::Serializer
  attributes :id, :title
  has_many :question
end
