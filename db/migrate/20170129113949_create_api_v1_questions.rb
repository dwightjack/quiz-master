class CreateApiV1Questions < ActiveRecord::Migration[5.0]
  def change
    create_table :api_v1_questions do |t|
      t.string :type
      t.string :body
      t.string :answer
      t.string :options
      t.belongs_to :quiz, foreign_key: true

      t.timestamps
    end
  end
end
