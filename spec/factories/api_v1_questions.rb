FactoryGirl.define do
  factory :api_v1_question, class: 'Api::V1::Question' do
    type ""
    body "MyString"
    answer "MyString"
    options "MyString"
    quiz nil
  end
end
