FactoryGirl.define do
  factory :api_v1_answer, class: 'Api::V1::Answer' do
    answer "MyString"
    timestamp 1
    question nil
    user nil
  end
end
